#!/bin/bash

#Test case parameters: cart_mass bar_mass bar_length sampling_period pertubation
#Test case 5: Light cart mass
./InvertedPendulum 3 10 70 0.5 12

#Balances with a cart mass of 3 grams and bar mass of 10 grams with a sampling peroid of 0.5

