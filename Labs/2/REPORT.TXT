CSC C85 - Fall 2017 - Lab #1 Report

Particle Filters for Robot Localization
______________________________________________________________________

Team name:

Name 1 (last, first): Matthew Kewarth
Student number 1: 1000553705

Name 2 (last, first): 
Student number 2:

Name 3 (last, first):
Student number 3:

(special: Lose marks for not completing the above information)
_______________________________________________________________________

Please note below what parts of your task were completed, and for
incomplete tasks, give a brief description of progress and how
you think the task could be completed

a) Implemented randomized particle initialization
    
    We created a new random particle by using the function drand48 to generate random
    value between 0 and 1 for x, y, and theta and scaled them appropiately. To be more specific:
        x = drand48() * sx
        y = drand48() * sy
        theta = drand48() * 360.0
    If the particle was found to be generated inside a wall we simply re-generated the value
    before adding the new particle to the list.

b) Implemented particle likelihood computation using Gaussian 
   noise model.
    
    We computed the difference between the ground readings of the particle and the sensor
    reading given by the robot and compute the gaussian evaluation on this difference
    with a varience of 20 (as specified in the starter code)
    Because these readings are split into an array of 16 elements we perform this process
    for each slice and sum the gaussian evaluations together.
    Finally, we define the new probability of the particle to the the sum of the evals multiplied
    with the particle's prior probability.
    

c) Implemented Step 1 in the starter code
    
    First we generate an action which is a random movement between 0 and 2.
    We then move each particle and the robot by this movement with respect to with their 
    orientation. If a particle has hit a wall, we generate a new orientation, theta, and
    attempt to take another random step in this new direction. If this results in no hit
    we keep this new theta and position, if not we repeat the process until we are no longer
    hitting a wall.


d) Implemented Step 3 in the starter code

    For each particle we compute the likelihood computation.
    We then normalize all newly computed probabilities.

e) Implemented Step 4 in the starter code
  
    
_______________________________________________________________________

Questions:

1) For each of the maps provided, give your estimate of the minimum
   number of particles required for reliably successful localization
   (i.e. the robot succeeds almost always).

2) What properties of the map make the localization task difficult?

    A map with identical rooms can create issues. If the robot is in one of these
    rooms then particles in another room may generate similar readings, resulting 
    in pockets of particle clusters that linger until the robot move of of the room.

3) What is the effect of a non-Gaussian noise model in the performance
   of the particle filter?


4) What would you do to improve the sensor model?

    Applying recursive filtering to 


5) The motion model is noisy. Each time the robot moves, the direction
   of motion is slightly altered in a random manner. This is why the
   robot wanders around instead of moving in a straight line.

   a) Is this something we can avoid in the real world?

    We do not believe it is possible to remove this noise due, even if we
    had a perfect sensors, as the physical limitations of the robot's motors 
    producing drift. 
  
   b) Is it a bug or a feature?

    It either dies young as a bug, or lives long enough to be feature.
   
   c) Would the particle filter work with noiseless motion?
      if yes, under what conditions...
      if no, why?

________________________________________________________________

Grading:

- Lab attendance and work			/15
- Particle Filter solution			/70  (includes style and commenting of code)
- Answers in this report			/15

Total for the session				/100 (*)

(*) Mark is conditional on all members of the team being able
    to explain any component of the solution when asked.
