/**************************************************************************
  CSC C85 - UTSC RoboSoccer AI core

  This file is where the actual planning is done and commands are sent
  to the robot.

  Please read all comments in this file, and add code where needed to
  implement your game playing logic. 

  Things to consider:

  - Plan - don't just react
  - Use the heading vectors!
  - Mind the noise (it's everywhere)
  - Try to predict what your oponent will do
  - Use feedback from the camera

  What your code should not do: 

  - Attack the opponent, or otherwise behave aggressively toward the
    oponent
  - Hog the ball (you can kick it, push it, or leave it alone)
  - Sit at the goal-line or inside the goal
  - Run completely out of bounds

  AI scaffold: Parker-Lee-Estrada, Summer 2013

  Version: 1.1 - Updated Jun 30, 2018 - F. Estrada
***************************************************************************/

#include "roboAI.h"			// <--- Look at this header file!

void clear_motion_flags(struct RoboAI *ai)
{
 // Reset all motion flags. See roboAI.h for what each flag represents
 // *You may or may not want to use these*
 ai->st.mv_fwd=0; 
 ai->st.mv_back=0;
 ai->st.mv_bl=0;
 ai->st.mv_br=0;
 ai->st.mv_fl=0;
 ai->st.mv_fr=0;
}

struct blob *id_coloured_blob2(struct RoboAI *ai, struct blob *blobs, int col)
{
 /////////////////////////////////////////////////////////////////////////////
 // This function looks for and identifies a blob with the specified colour.
 // It uses the hue and saturation values computed for each blob and tries to
 // select the blob that is most like the expected colour (red, green, or blue)
 //
 // If you find that tracking of blobs is not working as well as you'd like,
 // you can try to improve the matching criteria used in this function.
 // Remember you also have access to shape data and orientation axes for blobs.
 //
 // Inputs: The robot's AI data structure, a list of blobs, and a colour target:
 // Colour parameter: 0 -> Red
 //                   1 -> Yellow or Green (see below)
 //                   2 -> Blue
 // Returns: Pointer to the blob with the desired colour, or NULL if no such
 // 	     blob can be found.
 /////////////////////////////////////////////////////////////////////////////

 struct blob *p, *fnd;
 double vr_x,vr_y,maxfit,mincos,dp;
 double vb_x,vb_y,fit;
 double maxsize=0;
 double maxgray;
 int grayness;
 int i;
 
 maxfit=.025;                                             // Minimum fitness threshold
 mincos=.65;                                              // Threshold on colour angle similarity
 maxgray=.25;                                             // Maximum allowed difference in colour
                                                          // to be considered gray-ish (as a percentage
                                                          // of intensity)

 // For whatever reason, green printed paper tends to be problematic, so, we will use yellow instead,
 // but, the code below can be made to detect a green uniform if needed by commenting/uncommenting
 // the appropriate line just below.
 // The reference colour here is in the HSV colourspace, we look at the hue component, which is a
 // defined within a colour-wheel that contains all possible colours. Hence, the hue component
 // is a value in [0 360] degrees, or [0 2*pi] radians, indicating the colour's location on the
 // colour wheel. If we want to detect a different colour, all we need to do is figure out its
 // location in the colour wheel and then set the angles below (in radians) to that colour's
 // angle within the wheel.
 // For reference: Red is at 0 degrees, Yellow is at 60 degrees, Green is at 120, and Blue at 240.
 if (col==0) {vr_x=cos(0); vr_y=sin(0);} 	                                         // detect red
 else if (col==2) {vr_x=cos(2.0*PI*(60.0/360.0)); vr_y=sin(2.0*PI*(85/360.0));}        // detect yellow
// else if (col==1) {vr_x=cos(2.0*PI*(120.0/360.0)); vr_y=sin(2.0*PI*(120.0/360.0));}    // detect green
 else if (col==1) {vr_x=cos(2.0*PI*(240.0/360.0)); vr_y=sin(2.0*PI*(240.0/360.0));}      // detect blue


 // In what follows, colours are represented by a unit-length vector in the direction of the
 // hue for that colour. Similarity between two colours (e.g. a reference above, and a pixel's
 // or blob's colour) is measured as the dot-product between the corresponding colour vectors.
 // If the dot product is 1 the colours are identical (their vectors perfectly aligned), 
 // from there, the dot product decreases as the colour vectors start to point in different
 // directions. Two colours that are opposite will result in a dot product of -1.
 
 p=blobs;
 while (p!=NULL)
 { 
  if (p->size>maxsize) maxsize=p->size;
  p=p->next;
 }

 p=blobs;
 fnd=NULL;
 while (p!=NULL)
 {

  // Normalization and range extension
  vb_x=cos(p->H);
  vb_y=sin(p->H);

  dp=(vb_x*vr_x)+(vb_y*vr_y);                                       // Dot product between the reference color vector, and the
                                                                    // blob's color vector.

  fit=dp*p->S*p->S*(p->size/maxsize);                               // <<< --- This is the critical matching criterion.
                                                                    // * THe dot product with the reference direction,
                                                                    // * Saturation squared
                                                                    // * And blob size (in pixels, not from bounding box)
                                                                    // You can try to fine tune this if you feel you can
                                                                    // improve tracking stability by changing this fitness
                                                                    // computation

  // Check for a gray-ish blob - they tend to give trouble
  grayness=0;
  if (fabs(p->R-p->G)/p->R<maxgray&&fabs(p->R-p->G)/p->G<maxgray&&fabs(p->R-p->B)/p->R<maxgray&&fabs(p->R-p->B)/p->B<maxgray&&\
      fabs(p->G-p->B)/p->G<maxgray&&fabs(p->G-p->B)/p->B<maxgray) grayness=1;
  
  if (fit>maxfit&&dp>mincos&&grayness==0)
  {
   fnd=p;
   maxfit=fit;
  }
  
  p=p->next;
 }

 return(fnd);
}


void track_agents(struct RoboAI *ai, struct blob *blobs)
{
 ////////////////////////////////////////////////////////////////////////
 // This function does the tracking of each agent in the field. It looks
 // for blobs that represent the bot, the ball, and our opponent (which
 // colour is assigned to each bot is determined by a command line
 // parameter).
 // It keeps track within the robot's AI data structure of multiple 
 // parameters related to each agent:
 // - Position
 // - Velocity vector. Not valid while rotating, but possibly valid
 //   while turning.
 // - Heading (a unit vector in the direction of motion). Not valid
 //   while rotating - possibly valid while turning
 // - Pointers to the blob data structure for each agent
 //
 // This function will update the blob data structure with the velocity
 // and heading information from tracking. 
 //
 // NOTE: If a particular agent is not found, the corresponding field in
 //       the AI data structure (ai->st.ball, ai->st.self, ai->st.opp)
 //       will remain NULL. Make sure you check for this before you 
 //       try to access an agent's blob data! 
 //
 // In addition to this, if calibration data is available then this
 // function adjusts the Y location of the bot and the opponent to 
 // adjust for perspective projection error. See the handout on how
 // to perform the calibration process.
 //
 // Note that the blob data
 // structure itself contains another useful vector with the blob
 // orientation (obtained directly from the blob shape, valid at all
 // times even under rotation, but can be pointing backward!)
 //
 // This function receives a pointer to the robot's AI data structure,
 // and a list of blobs.hearthstone
 //
 // You can change this function if you feel the tracking is not stable.
 // First, though, be sure to completely understand what it's doing.
 /////////////////////////////////////////////////////////////////////////

 struct blob *p;
 double mg,vx,vy,pink,doff,dmin,dmax,adj;
 double NOISE_VAR=5;

 // Reset ID flags
 ai->st.ballID=0;
 ai->st.selfID=0;
 ai->st.oppID=0;
 ai->st.ball=NULL;			// Be sure you check these are not NULL before
 ai->st.self=NULL;			// trying to access data for the ball/self/opponent!
 ai->st.opp=NULL;

 // Find the ball
 p=id_coloured_blob2(ai,blobs,2);
 if (p)
 {
  ai->st.ball=p;			// New pointer to ball
  ai->st.ballID=1;			// Set ID flag for ball (we found it!)
  ai->st.bvx=p->cx-ai->st.old_bcx;	// Update ball velocity in ai structure and blob structure
  ai->st.bvy=p->cy-ai->st.old_bcy;
  ai->st.ball->vx=ai->st.bvx;
  ai->st.ball->vy=ai->st.bvy;

  ai->st.old_bcx=p->cx; 		// Update old position for next frame's computation
  ai->st.old_bcy=p->cy;
  ai->st.ball->idtype=3;

  vx=ai->st.bvx;			// Compute heading direction (normalized motion vector)
  vy=ai->st.bvy;
  mg=sqrt((vx*vx)+(vy*vy));
  if (mg>NOISE_VAR)			// Update heading vector if meaningful motion detected
  {
   vx/=mg;
   vy/=mg;
   ai->st.bmx=vx;
   ai->st.bmy=vy;
  }
  ai->st.ball->mx=ai->st.bmx;
  ai->st.ball->my=ai->st.bmy;
 }
 else {
  ai->st.ball=NULL;
 }
 
 // ID our bot
 if (ai->st.botCol==0) p=id_coloured_blob2(ai,blobs,1);
 else p=id_coloured_blob2(ai,blobs,0);
 if (p!=NULL&&p!=ai->st.ball)
 {
  ai->st.self=p;			// Update pointer to self-blob

  // Adjust Y position if we have calibration data
  if (fabs(p->adj_Y[0][0])>.1)
  {
   dmax=384.0-p->adj_Y[0][0];
   dmin=767.0-p->adj_Y[1][0];
   pink=(dmax-dmin)/(768.0-384.0);
   adj=dmin+((p->adj_Y[1][0]-p->cy)*pink);
   p->cy=p->cy+adj;
   if (p->cy>767) p->cy=767;
   if (p->cy<1) p->cy=1;
  }

  ai->st.selfID=1;
  ai->st.svx=p->cx-ai->st.old_scx;
  ai->st.svy=p->cy-ai->st.old_scy;
  ai->st.self->vx=ai->st.svx;
  ai->st.self->vy=ai->st.svy;

  ai->st.old_scx=p->cx; 
  ai->st.old_scy=p->cy;
  ai->st.self->idtype=1;

  vx=ai->st.svx;
  vy=ai->st.svy;
  mg=sqrt((vx*vx)+(vy*vy));
  if (mg>NOISE_VAR)
  {
   vx/=mg;
   vy/=mg;
   ai->st.smx=vx;
   ai->st.smy=vy;
  }

  ai->st.self->mx=ai->st.smx;
  ai->st.self->my=ai->st.smy;
 }
 else ai->st.self=NULL;

 // ID our opponent
 if (ai->st.botCol==0) p=id_coloured_blob2(ai,blobs,0);
 else p=id_coloured_blob2(ai,blobs,1);
 if (p!=NULL&&p!=ai->st.ball&&p!=ai->st.self)
 {
  ai->st.opp=p;	

  if (fabs(p->adj_Y[0][1])>.1)
  {
   dmax=384.0-p->adj_Y[0][1];
   dmin=767.0-p->adj_Y[1][1];
   pink=(dmax-dmin)/(768.0-384.0);
   adj=dmin+((p->adj_Y[1][1]-p->cy)*pink);
   p->cy=p->cy+adj;
   if (p->cy>767) p->cy=767;
   if (p->cy<1) p->cy=1;
  }

  ai->st.oppID=1;
  ai->st.ovx=p->cx-ai->st.old_ocx;
  ai->st.ovy=p->cy-ai->st.old_ocy;
  ai->st.opp->vx=ai->st.ovx;
  ai->st.opp->vy=ai->st.ovy;

  ai->st.old_ocx=p->cx; 
  ai->st.old_ocy=p->cy;
  ai->st.opp->idtype=2;

  vx=ai->st.ovx;
  vy=ai->st.ovy;
  mg=sqrt((vx*vx)+(vy*vy));
  if (mg>NOISE_VAR)
  {
   vx/=mg;
   vy/=mg;
   ai->st.omx=vx;
   ai->st.omy=vy;
  }
  ai->st.opp->mx=ai->st.omx;
  ai->st.opp->my=ai->st.omy;
 }
 else ai->st.opp=NULL;

}

void id_bot(struct RoboAI *ai, struct blob *blobs)
{
 ///////////////////////////////////////////////////////////////////////////////
 // ** DO NOT CHANGE THIS FUNCTION **
 // This routine calls track_agents() to identify the blobs corresponding to the
 // robots and the ball. It commands the bot to move forward slowly so heading
 // can be established from blob-tracking.
 //
 // NOTE 1: All heading estimates, velocity vectors, position, and orientation
 //         are noisy. Remember what you have learned about noise management.
 //
 // NOTE 2: Heading and velocity estimates are not valid while the robot is
 //         rotating in place (and the final heading vector is not valid either).
 //         To re-establish heading, forward/backward motion is needed.
 //
 // NOTE 3: However, you do have a reliable orientation vector within the blob
 //         data structures derived from blob shape. It points along the long
 //         side of the rectangular 'uniform' of your bot. It is valid at all
 //         times (even when rotating), but may be pointing backward and the
 //         pointing direction can change over time.
 //
 // You should *NOT* call this function during the game. This is only for the
 // initialization step. Calling this function during the game will result in
 // unpredictable behaviour since it will update the AI state.double sdrive[2];
 ///////////////////////////////////////////////////////////////////////////////
 
 struct blob *p;
 static double stepID=0;
 double frame_inc=1.0/5.0;

 BT_drive(LEFT_MOTOR, RIGHT_MOTOR, 30);			// Start forward motion to establish heading
					// Will move for a few frames.

 track_agents(ai,blobs);		// Call the tracking function to find each agent

 if (ai->st.selfID==1&&ai->st.self!=NULL)
  fprintf(stderr,"Successfully identified self blob at (%f,%f)\n",ai->st.self->cx,ai->st.self->cy);
 if (ai->st.oppID==1&&ai->st.opp!=NULL)
  fprintf(stderr,"Successfully identified opponent blob at (%f,%f)\n",ai->st.opp->cx,ai->st.opp->cy);
 if (ai->st.ballID==1&&ai->st.ball!=NULL)
  fprintf(stderr,"Successfully identified ball blob at (%f,%f)\n",ai->st.ball->cx,ai->st.ball->cy);

 stepID+=frame_inc;
 if (stepID>=1&&ai->st.selfID==1)	// Stop after a suitable number of frames.
 {
  ai->st.state+=1;
  stepID=0;
  BT_all_stop(0);
 }
 else if (stepID>=1) stepID=0;

 // At each point, each agent currently in the field should have been identified.
 return;
}

int setupAI(int mode, int own_col, struct RoboAI *ai)
{
 /////////////////////////////////////////////////////////////////////////////
 // ** DO NOT CHANGE THIS FUNCTION **
 // This sets up the initial AI for the robot. There are three different modes:
 //
 // SOCCER -> Complete AI, tries to win a soccer game against an opponentint filter_sonar_dist
 // PENALTY -> Score a goal (no goalie!)
 // CHASE -> Kick the ball and chase it around the field
 //
 // Each mode sets a different initial state (0, 100, 200). Hence, 
 // AI states for SOCCER will be 0 through 99
 // AI states for PENALTY will be 100 through 199
 // AI states for CHASE will be 200 through 299
 //
 // You will of course have to add code to the AI_main() routine to handle
 // each mode's states and do the right thing.
 //
 // Your bot should not become confused about what mode it started in!
 //////////////////////////////////////////////////////////////////////////////        

 switch (mode) {
 case AI_SOCCER:
	fprintf(stderr,"Standard Robo-Soccer mode requested\n");
        ai->st.state=0;		// <-- Set AI initial state to 0
        break;
 case AI_PENALTY:
	fprintf(stderr,"Penalty mode! let's kick it!\n");
	ai->st.state=100;	// <-- Set AI initial state to 100
        break;
 case AI_CHASE:
	fprintf(stderr,"Chasing the ball...\n");
	ai->st.state=200;	// <-- Set AI initial state to 200
        break;	
 default:
	fprintf(stderr, "AI mode %d is not implemented, setting mode to SOCCER\n", mode);
	ai->st.state=0;
	}

 BT_all_stop(0);			// Stop bot,
 ai->runAI = AI_main;		// and initialize all remaining AI data
 ai->calibrate = AI_calibrate;
 ai->st.ball=NULL;
 ai->st.self=NULL;
 ai->st.opp=NULL;
 ai->st.side=0;
 ai->st.botCol=own_col;
 ai->st.old_bcx=0;
 ai->st.old_bcy=0;
 ai->st.old_scx=0;
 ai->st.old_scy=0;
 ai->st.old_ocx=0;
 ai->st.old_ocy=0;
 ai->st.bvx=0;
 ai->st.bvy=0;
 ai->st.svx=0;
 ai->st.svy=0;
 ai->st.ovx=0;
 ai->st.ovy=0;
 ai->st.selfID=0;
 ai->st.oppID=0;
 ai->st.ballID=0;
 clear_motion_flags(ai);
 fprintf(stderr,"Initialized!\n");

 return(1);
}

void AI_calibrate(struct RoboAI *ai, struct blob *blobs)
{
 // Basic colour blob tracking loop for calibration of vertical offset
 // See the handout for the sequence of steps needed to achieve calibration.
 track_agents(ai,blobs);
}

void AI_main(struct RoboAI *ai, struct blob *blobs, void *state)
{
 /*************************************************************************
  This is the main AI loop.
  
  It is called by the imageCapture code *once* per frame. And it *must not*
  enter a loop or wait for visual events, since no visual refresh will happen
  until this call returns!
  
  Therefore. Everything you do in here must be based on the states in your
  AI and the actions the robot will perform must be started or stopped 
  depending on *state transitions*. 

  E.g. If your robot is currently standing still, with state = 03, and
   your AI determines it should start moving forward and transition to
   state 4. Then what you must do is 
   - send a command to start forward motion at the desired speed
   - update the robot's state
   - return
  
  I can not emphasize this enough. Unless this call returns, no image
  processing will occur, no new information will be processed, and your
  bot will be stuck on its last action/state.

  You will be working with a state-based AI. You are free to determine
  how many states there will be, what each state will represent, and
  what actions the robot will perform based on the state as well as the
  state transitions.

  You must *FULLY* document your state representation in the report

  The first two states for each more are already defined:
  State 0,100,200 - Before robot ID has taken place (this state is the initial
            	    state, or is the result of pressing 'r' to reset the AI)
  State 1,101,201 - State after robot ID has taken place. At this point the AI
            	    knows where the robot is, as well as where the opponent and
            	    ball are (if visible on the playfield)

  Relevant UI keyboard commands:
  'r' - reset the AI. Will set AI state to zero and re-initialize the AI
	data structure.
  't' - Toggle the AI routine (i.e. start/stop calls to AI_main() ).
  'o' - Robot immaskcorrect_heading(ai)redditediate all-stop! - do not allow your NXT to get damaged!

  ** Do not change the behaviour of the robot ID routine **
 **************************************************************************/

 // change to the ports representing the left and right motors
 char lport=MOTOR_A;
 char rport=MOTOR_B;

 if (ai->st.state==0||ai->st.state==100||ai->st.state==200)  	// Initial set up - find own, ball, and opponent blobs
 {
  // Carry out self id process.
  fprintf(stderr,"Initial state, self-id in progress...\n");
  id_bot(ai,blobs);
  if ((ai->st.state%100)!=0)	// The id_bot() routine will change the AI state to initial state + 1
  {				// if robot identification is successful.
   if (ai->st.self->cx>=512) ai->st.side=1; else ai->st.side=0;
   BT_all_stop(0);
   clear_motion_flags(ai);
   fprintf(stderr,"Self-ID complete. Current position: (%f,%f), current heading: [%f, %f], AI state=%d\n",ai->st.self->cx,ai->st.self->cy,ai->st.self->mx,ai->st.self->my,ai->st.state);
  }
 }
 else
 {
  /****************************************************************************
   TO DO:
   You will need to replace this 'catch-all' code with actual program logic to
   implement your bot's state-based AI.

   After id_bot() has successfully completed its work, the state should be
   1 - if the bot is in SOCCER mode
   101 - if the bot is in PENALTY mode
   201 - if the bot is in CHASE mode

   Your AI code needs to handle these states and their associated state
   transitions which will determine the robot's behaviour for each mode.

   Please note that in this function you should add appropriate functions below
   to handle each state's processing, and the code here should mostly deal with
   state transitions and with calling the appropriate function based on what
   the bot is supposed to be doing.
  *****************************************************************************/
  //fprintf(stderr,"Just trackin'!\n");	// bot, opponent, and ball.
  track_agents(ai,blobs);		// Currently, does nothing but endlessly track
  static double aligned_y;
  double fy = 1.0;
  static int f = 1;
  static double osmx;
  static double osmy;
  static double new_dx;
  static double new_dy;
  switch (ai->st.state){
    case 1:
    {
        // PENALTY mode start
        
        // Initialize Bot.
        fprintf(stderr, "PENALTY mode\n");
        BT_drive(MOTOR_C, MOTOR_D, 20);
        if(ai->st.ballID && ai->st.selfID){
	    if(ai->st.oppID){
		if(detect_collision(ai)){
		    break;
		}
	    }
            if(ai->st.svx == 0.0 && ai->st.svy == 0.0){ 
		        fprintf(stderr, "Im wasnt moving this frame\n");
		        BT_all_stop(1); 
		        break;
            } else{
                aligned_y = compute_kick_position(ai); 
		if(aligned_y >= 670){
		    aligned_y = 670;
		}
		else if(aligned_y < 50){
		    aligned_y = 50;
		}
                fprintf(stderr, "My planned alignment is: %f", aligned_y);
                fprintf(stderr, "-----------\nMy heading is: [%f, %f]\n-----------\n", ai->st.smx, ai->st.smy);
                double s = correct_heading(ai);
                new_dx = s * ai->st.self->dx;
                new_dy = s * ai->st.self->dy;
		        osmx = new_dx;
		        osmy = new_dy;
                fprintf(stderr, "Heading x = %f\n", osmx);
                fprintf(stderr, "Heading y = %f\n", osmy);
                fprintf(stderr, "Direction x = %f\n", new_dx);
                fprintf(stderr, "Direction y = %f\n", new_dy);
            }
	        fprintf(stderr, "Initialization is done\n");
            BT_all_stop(1);
            ai->st.state = 2;
        } else {
	      fprintf(stderr, "I couldnt find myself or the ball\n");
          BT_all_stop(1);
        }
        break;
    }
    case 2:
    {
        double head = 1.0;
        double fy = 1.0;
        double bx, by;
        fprintf(stderr, "X = %f, Y = %f\n", new_dx, new_dy);
        if(ai->st.selfID){
           new_dx = ai->st.self->dx;
           new_dy = ai->st.self->dy;
           
           if(dot_product(new_dx, new_dy, osmx, osmy) < 0.0){
               new_dx = -1.0 * new_dx;
               new_dy = -1.0 * new_dy;
           }
           osmx = new_dx;
           osmy = new_dy;
        }
        else{
            new_dx = osmx;
            new_dy = osmy;
        }
        if(ai->st.ballID){
            bx = ai->st.ball->cx;
            by = ai->st.ball->cy;
        }
        else{
            bx = ai->st.old_bcx;
            by = ai->st.old_bcy;
        }
        /*if(new_dy > by){
            fy = 1.0;
        }*/
        fprintf(stderr, "Bot Y = %f Aligned Y = %f\n", new_dy, aligned_y);
        if (ai->st.self->cy > aligned_y) {
           fy = -1.0;
        } /*else {
            fy = -1.0;
        }*/
        fprintf(stderr, "OSMX = %f\n", osmx);
        fprintf(stderr, "OSMY = %f\n", osmy);
        fprintf(stderr, "I want to align to: [%f, %f]\n", 0.0, fy);
        fprintf(stderr, "Cross Product: %f\n", cross_product(new_dx, new_dy, 0.0, fy));
        fprintf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, 0.0, fy));
        if(cross_product(new_dx, new_dy, 0.0, fy) < 0.0){		
	        // Ball on left side. Rotate to left.
            fprintf(stderr, "Ball on left side\n");
            if (dot_product(new_dx, new_dy,0.0, fy) <= 0.95) {
                //Keep rotating.
                
                BT_turn(MOTOR_C, -40, MOTOR_D, 40);  
            } else {
                fprintf(stderr, "Going to state 3");
                ai->st.state = 3;
                break;  
            }
        } else {
            // Ball on right side. 
            fprintf(stderr, "Ball on right side\n");
            if (dot_product(new_dx, new_dy, 0.0, fy) <= 0.95) {
                //Keep rotating.
                BT_turn(MOTOR_C, 40, MOTOR_D, -40);   
            } else {
                fprintf(stderr, "Going to state 3");
                ai->st.state = 3;
                break;
            }
        }           
        break;
    }
    case 3:
    {
        // Drive up to aligned_y
        static double sx, sy;
        if(ai->st.ballID && ai->st.selfID){
            double p = ai->st.self->cy - aligned_y;
            fprintf(stderr, "P = %f\n", p);
            if(abs(p) <= 18.0){
                BT_all_stop(1);
                ai->st.state = 4;
                break;
            }
            double dx, dy, m;
            if(!sx && !sy){
                sx = ai->st.smx;
                sy = ai->st.smy;
            }
            else{
                sx = 0.50 * ai->st.smx + 0.50 * ai->st.smx;
                sy = 0.50 * ai->st.smy + 0.50 * ai->st.smy;
            }

            dx = 0.0;
            dy = 1.0;
            if(ai->st.self->cy < aligned_y) dy = -1.0;
            m = magnitude(dx, dy);
            double mdx = dx / magnitude(dx, dy);
            double mdy = dy / magnitude(dx, dy);
            fprintf(stderr, "SX = %f\n", sx);
            fprintf(stderr, "SY = %f\n", sy);
            fprintf(stderr, "DX = %f\n", mdx);
            fprintf(stderr, "DY = %f\n", mdy);
            drive(sx, sy, mdx, mdy);
        }
        else BT_all_stop(1);
        break;
    }
    case 4:
    {
        //Rotate to align to ball-goal 
        static double sx, sy;
	    double dot_p = dot_product(new_dx, new_dy, osmx, osmy);
        double head = 1.0;
        if(ai->st.ballID && ai->st.selfID){
           new_dx = ai->st.self->dx;
           new_dy = ai->st.self->dy;
           double b_to_self_x = (1.0 - ai->st.side)*1028 - ai->st.ball->cx;
           double b_to_self_y = (0.5 * 720) - ai->st.ball->cy;
           fprintf(stderr, "Old VX = %f, Old VY = %f\n", b_to_self_x, b_to_self_y);
           fprintf(stderr, "Magnitude = %f\n", magnitude(b_to_self_x, b_to_self_y));
           double mag_y = b_to_self_y / magnitude(b_to_self_x, b_to_self_y);
           fprintf(stderr, "Magnitude_Y = %f\n", mag_y);
           b_to_self_x = b_to_self_x / magnitude(b_to_self_x, b_to_self_y);
           if(dot_product(new_dx, new_dy, osmx, osmy) < 0.0){
               new_dx = -1.0 * new_dx;
               new_dy = -1.0 * new_dy;
           }
           osmx = new_dx;
	       osmy = new_dy;
           fprintf(stderr, "DX = %f, DY = %f\n", new_dx, new_dy);
           fprintf(stderr, "VX = %f, VY = %f\n", b_to_self_x, mag_y);
           fprintf(stderr, "My dot product: %f\n", dot_product(new_dx, new_dy, b_to_self_x, mag_y)); 
           if(cross_product(new_dx, new_dy, b_to_self_x, mag_y) < 0.0){		
	         // Ball on left side. Rotate to left.
             
             if (dot_product(new_dx, new_dy, b_to_self_x, mag_y) <= 0.95) {
                 //Keep rotating.
                 
                 BT_turn(MOTOR_C, -40, MOTOR_D, 40);
                 
              } else {
                  fprintf(stderr, "Going to state 5\n");
                  BT_all_stop(0);
                  ai->st.state = 5;
                 
                  
              }
           } else {
             // Ball on right side. 
             
             if (dot_product(new_dx, new_dy, b_to_self_x, mag_y) <= 0.95) {
                 //Keep rotating.
                 BT_turn(MOTOR_C, 40, MOTOR_D, -40);
                 
              } else {
                  fprintf(stderr, "Going to state 5\n");
		          BT_all_stop(0);
                  ai->st.state = 5;
                 
              }
           }           
           
        }
        break;
    }
    case 5:
    {
        BT_motor_port_start(MOTOR_B, 100);
        //Drive to Ball
        static double sx, sy;
        if(ai->st.ballID && ai->st.selfID){
            double dx, dy, m;
            dx = ai->st.ball->cx - ai->st.self->cx;
            dy = ai->st.ball->cx - ai->st.self->cx;
            m = magnitude(dx, dy);
            if(m >= 300.0){
                if(!sx && !sy){
                  sx = ai->st.smx;
                  sy = ai->st.smy;
                }
                else {
                  sx = 0.50 * ai->st.smx + 0.50 * ai->st.smx;
                  sy = 0.50 * ai->st.smy + 0.50 * ai->st.smy;
                }

            

            
            
                double mdx = dx / magnitude(dx, dy);
                double mdy = dy / magnitude(dx, dy);
                fprintf(stderr, "SX = %f\n", sx);
                fprintf(stderr, "SY = %f\n", sy);
                fprintf(stderr, "DX = %f\n", mdx);
                fprintf(stderr, "DY = %f\n", mdy);
                drive(sx, sy, mdx, mdy);
            } else {
               fprintf(stderr, "Going to state 9");
               ai->st.state = 6;
               break;
            }
        }
        break;
    }
    case 6:
    {
        if(ai->st.ballID && ai->st.selfID){
          // Distance to ball is close. Kick ball.
          BT_motor_port_start(MOTOR_B, 100);
          BT_drive(MOTOR_C, MOTOR_D, 100);
          BT_timed_motor_port_start_v2(MOTOR_C, 100, 1000);
          //Cool down after kicking ball
          BT_drive(MOTOR_C, MOTOR_D, 30);
          BT_timed_motor_port_start_v2(MOTOR_C, 30, 100);
          ai->st.state = 7;
          
        }
        break;
    }
    case 7:
    {
        double dot_p = dot_product(new_dx, new_dy, osmx, osmy);
        double head = 1.0;
        fprintf(stderr, "X = %f, Y = %f\n", new_dx, new_dy);
        if(ai->st.ballID && ai->st.selfID){
           new_dx = ai->st.self->dx;
           new_dy = ai->st.self->dy;
           double b_to_self_x = ai->st.ball->cx - ai->st.self->cx;
           double b_to_self_y = ai->st.ball->cy - ai->st.self->cy;
           b_to_self_x = b_to_self_x / magnitude(b_to_self_x, b_to_self_y);
           b_to_self_y = b_to_self_y / magnitude(b_to_self_x, b_to_self_y);
           if(dot_product(new_dx, new_dy, osmx, osmy) < 0.0){
               new_dx = -1.0 * new_dx;
               new_dy = -1.0 * new_dy;
           }
           osmx = new_dx;
	       osmy = new_dy;
           //fprintf(stderr, "OSMX = %f\n", osmx);
           //fprintf(stderr, "OSMY = %f\n", osmy);
           //fprintf(stderr, "I want to align to: [%f, %f]|\n", b_to_self_x, b_to_self_y);
           //fprintf(stderr, "Cross Product: %f\n", cross_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
           //fprintf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
           if(cross_product(new_dx, new_dy, b_to_self_x, b_to_self_y) < 0.0){		
	         // Ball on left side. Rotate to left.
             //fprintf(stderr, "Ball on left side\n");
             if (dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y) <= 0.95) {
                 //Keep rotating.
                 //printf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
                 BT_turn(MOTOR_C, -40, MOTOR_D, 40);
                 
              } else {
                  fprintf(stderr, "Going to state 8");
                  ai->st.state = 8;
                  break;
                  
              }
           } else {
             // Ball on right side. 
             //fprintf(stderr, "Ball on right side\n");
             if (dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y) <= 0.95) {
                 //Keep rotating.
                 //fprintf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
                 BT_turn(MOTOR_C, 40, MOTOR_D, -40);
                 
              } else {
                  fprintf(stderr, "Going to state 8");
                  ai->st.state = 8;
                  break;
              }
           }           
           
        }
        
        break;
    }
    case 8:
    {
        //Drive to Ball
        static double sx, sy;
        if(ai->st.ballID && ai->st.selfID){
        fprintf(stderr, "Wall detected %d\n", detect_wall_collision(ai));
	    if(detect_wall_collision(ai)){ 
		   ai->st.state = 7;
		   break;
	    } else {
            fprintf(stderr, "Position X: %f, Y: %f\n", ai->st.self->cx, ai->st.self->cy);
	        BT_motor_port_start(MOTOR_B, 100);
            double dx, dy, m;
            dx = ai->st.ball->cx - ai->st.self->cx;
            dy = ai->st.ball->cx - ai->st.self->cx;
            m = magnitude(dx, dy);
            if(m >= 300.0){
                if(!sx && !sy){
                  sx = ai->st.smx;
                  sy = ai->st.smy;
                }
                else {
                  sx = 0.50 * ai->st.smx + 0.50 * ai->st.smx;
                  sy = 0.50 * ai->st.smy + 0.50 * ai->st.smy;
                }

            

            
            
                double mdx = dx / magnitude(dx, dy);
                double mdy = dy / magnitude(dx, dy);
                fprintf(stderr, "SX = %f\n", sx);
                fprintf(stderr, "SY = %f\n", sy);
                fprintf(stderr, "DX = %f\n", mdx);
                fprintf(stderr, "DY = %f\n", mdy);
                drive(sx, sy, mdx, mdy);
            } else {
               fprintf(stderr, "Going to state 9");
               ai->st.state = 9;
               break;
            }
          }
            
        }
        break;
        /*if(ai->st.ballID && ai->st.selfID){
          //Find magnitude of bot to ball
          BT_motor_port_start(MOTOR_B, 100);
          double dist_to_ball = magnitude2(ai->st.self->cx, ai->st.self->cy, ai->st.ball->cx, ai->st.self->cy);
          fprintf(stderr, "Dist to ball: %f\n", dist_to_ball);
          if (dist_to_ball >= 350.0) {
             //Go to ball
             BT_drive(MOTOR_C, MOTOR_D, 100);
          } else {
             ai->st.state = 9;
          }
          break;
        } else {
          ai->st.state = 7;
        }*/

    }
    case 9:
    {
        if(ai->st.ballID && ai->st.selfID){
          // Distance to ball is close. Kick ball.
          BT_motor_port_start(MOTOR_B, 100);
          BT_drive(MOTOR_C, MOTOR_D, 100);
          BT_timed_motor_port_start_v2(MOTOR_C, 100, 500);
          //Cool down after kicking ball
          BT_drive(MOTOR_C, MOTOR_D, 30);
          BT_timed_motor_port_start_v2(MOTOR_C, 30, 100);
          ai->st.state = 7;
        }
        break;
    }
    case 101:
    {
        // PENALTY mode start
        
        // Initialize Bot.
        fprintf(stderr, "PENALTY mode\n");
        BT_drive(MOTOR_C, MOTOR_D, 20);
        if(ai->st.ballID && ai->st.selfID){
            if(ai->st.svx == 0.0 && ai->st.svy == 0.0){ 
		        fprintf(stderr, "Im wasnt moving this frame\n");
		        BT_all_stop(1); 
		        break;
            } else{
                aligned_y = compute_kick_position(ai); 
                fprintf(stderr, "My planned alignment is: %f", aligned_y);
                fprintf(stderr, "-----------\nMy heading is: [%f, %f]\n-----------\n", ai->st.smx, ai->st.smy);
                double s = correct_heading(ai);
                new_dx = s * ai->st.self->dx;
                new_dy = s * ai->st.self->dy;
		        osmx = new_dx;
		        osmy = new_dy;
                fprintf(stderr, "Heading x = %f\n", osmx);
                fprintf(stderr, "Heading y = %f\n", osmy);
                fprintf(stderr, "Direction x = %f\n", new_dx);
                fprintf(stderr, "Direction y = %f\n", new_dy);
            }
	        fprintf(stderr, "Initialization is done\n");
            BT_all_stop(1);
            ai->st.state = 102;
        } else {
	      fprintf(stderr, "I couldnt find myself or the ball\n");
          BT_all_stop(1);
        }
        break;
    }
    case 102:
    {
        double head = 1.0;
        double fy = 1.0;
        double bx, by;
        fprintf(stderr, "X = %f, Y = %f\n", new_dx, new_dy);
        if(ai->st.selfID){
           new_dx = ai->st.self->dx;
           new_dy = ai->st.self->dy;
           
           if(dot_product(new_dx, new_dy, osmx, osmy) < 0.0){
               new_dx = -1.0 * new_dx;
               new_dy = -1.0 * new_dy;
           }
           osmx = new_dx;
           osmy = new_dy;
        }
        else{
            new_dx = osmx;
            new_dy = osmy;
        }
        if(ai->st.ballID){
            bx = ai->st.ball->cx;
            by = ai->st.ball->cy;
        }
        else{
            bx = ai->st.old_bcx;
            by = ai->st.old_bcy;
        }
        /*if(new_dy > by){
            fy = 1.0;
        }*/
        fprintf(stderr, "Bot Y = %f Aligned Y = %f\n", new_dy, aligned_y);
        if (ai->st.self->cy > aligned_y) {
           fy = -1.0;
        } /*else {
            fy = -1.0;
        }*/
        fprintf(stderr, "OSMX = %f\n", osmx);
        fprintf(stderr, "OSMY = %f\n", osmy);
        fprintf(stderr, "I want to align to: [%f, %f]\n", 0.0, fy);
        fprintf(stderr, "Cross Product: %f\n", cross_product(new_dx, new_dy, 0.0, fy));
        fprintf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, 0.0, fy));
        if(cross_product(new_dx, new_dy, 0.0, fy) < 0.0){		
	        // Ball on left side. Rotate to left.
            fprintf(stderr, "Ball on left side\n");
            if (dot_product(new_dx, new_dy,0.0, fy) <= 0.95) {
                //Keep rotating.
                
                BT_turn(MOTOR_C, -40, MOTOR_D, 40);  
            } else {
                fprintf(stderr, "Going to state 103");
                ai->st.state = 103;
                break;  
            }
        } else {
            // Ball on right side. 
            fprintf(stderr, "Ball on right side\n");
            if (dot_product(new_dx, new_dy, 0.0, fy) <= 0.95) {
                //Keep rotating.
                BT_turn(MOTOR_C, 40, MOTOR_D, -40);   
            } else {
                fprintf(stderr, "Going to state 3");
                ai->st.state = 103;
                break;
            }
        }           
        break;
    }
    case 103:
    {
        // Drive up to aligned_y
        static double sx, sy;
        if(ai->st.ballID && ai->st.selfID){
            double p = ai->st.self->cy - aligned_y;
            fprintf(stderr, "P = %f\n", p);
            if(abs(p) <= 18.0){
                BT_all_stop(1);
                ai->st.state = 104;
                break;
            }
            double dx, dy, m;
            if(!sx && !sy){
                sx = ai->st.smx;
                sy = ai->st.smy;
            }
            else{
                sx = 0.50 * ai->st.smx + 0.50 * ai->st.smx;
                sy = 0.50 * ai->st.smy + 0.50 * ai->st.smy;
            }

            dx = 0.0;
            dy = 1.0;
            if(ai->st.self->cy < aligned_y) dy = -1.0;
            m = magnitude(dx, dy);
            double mdx = dx / magnitude(dx, dy);
            double mdy = dy / magnitude(dx, dy);
            fprintf(stderr, "SX = %f\n", sx);
            fprintf(stderr, "SY = %f\n", sy);
            fprintf(stderr, "DX = %f\n", mdx);
            fprintf(stderr, "DY = %f\n", mdy);
            drive(sx, sy, mdx, mdy);
        }
        else BT_all_stop(1);
        break;
    }
    case 104:
    {
        //Rotate to align to ball-goal 
        static double sx, sy;
	    double dot_p = dot_product(new_dx, new_dy, osmx, osmy);
        double head = 1.0;
        if(ai->st.ballID && ai->st.selfID){
           new_dx = ai->st.self->dx;
           new_dy = ai->st.self->dy;
           double b_to_self_x = (1.0 - ai->st.side)*1028 - ai->st.ball->cx;
           double b_to_self_y = (0.5 * 720) - ai->st.ball->cy;
           fprintf(stderr, "Old VX = %f, Old VY = %f\n", b_to_self_x, b_to_self_y);
           fprintf(stderr, "Magnitude = %f\n", magnitude(b_to_self_x, b_to_self_y));
           double mag_y = b_to_self_y / magnitude(b_to_self_x, b_to_self_y);
           fprintf(stderr, "Magnitude_Y = %f\n", mag_y);
           b_to_self_x = b_to_self_x / magnitude(b_to_self_x, b_to_self_y);
           if(dot_product(new_dx, new_dy, osmx, osmy) < 0.0){
               new_dx = -1.0 * new_dx;
               new_dy = -1.0 * new_dy;
           }
           osmx = new_dx;
	       osmy = new_dy;
           fprintf(stderr, "DX = %f, DY = %f\n", new_dx, new_dy);
           fprintf(stderr, "VX = %f, VY = %f\n", b_to_self_x, mag_y);
           fprintf(stderr, "My dot product: %f\n", dot_product(new_dx, new_dy, b_to_self_x, mag_y)); 
           if(cross_product(new_dx, new_dy, b_to_self_x, mag_y) < 0.0){		
	         // Ball on left side. Rotate to left.
             
             if (dot_product(new_dx, new_dy, b_to_self_x, mag_y) <= 0.95) {
                 //Keep rotating.
                 
                 BT_turn(MOTOR_C, -40, MOTOR_D, 40);
                 
              } else {
                  fprintf(stderr, "Going to state 105\n");
                  BT_all_stop(0);
                  ai->st.state = 105;
                 
                  
              }
           } else {
             // Ball on right side. 
             
             if (dot_product(new_dx, new_dy, b_to_self_x, mag_y) <= 0.95) {
                 //Keep rotating.
                 BT_turn(MOTOR_C, 40, MOTOR_D, -40);
                 
              } else {
                  fprintf(stderr, "Going to state 105\n");
		          BT_all_stop(0);
                  ai->st.state = 105;
                 
              }
           }           
           
        }
        break;
    }
    case 105:
    {
        BT_motor_port_start(MOTOR_B, 100);
        //Drive to Ball
        static double sx, sy;
        if(ai->st.ballID && ai->st.selfID){
            double dx, dy, m;
            dx = ai->st.ball->cx - ai->st.self->cx;
            dy = ai->st.ball->cx - ai->st.self->cx;
            m = magnitude(dx, dy);
            if(m >= 300.0){
                if(!sx && !sy){
                  sx = ai->st.smx;
                  sy = ai->st.smy;
                }
                else {
                  sx = 0.50 * ai->st.smx + 0.50 * ai->st.smx;
                  sy = 0.50 * ai->st.smy + 0.50 * ai->st.smy;
                }

            

            
            
                double mdx = dx / magnitude(dx, dy);
                double mdy = dy / magnitude(dx, dy);
                fprintf(stderr, "SX = %f\n", sx);
                fprintf(stderr, "SY = %f\n", sy);
                fprintf(stderr, "DX = %f\n", mdx);
                fprintf(stderr, "DY = %f\n", mdy);
                drive(sx, sy, mdx, mdy);
            } else {

               fprintf(stderr, "Going to state 106");
               ai->st.state = 106;
               break;
            }
        }
        break;
    }
    case 106:
    {
        if(ai->st.ballID && ai->st.selfID){
          // Distance to ball is close. Kick ball.
          BT_motor_port_start(MOTOR_B, 100);
          BT_drive(MOTOR_C, MOTOR_D, 100);
          BT_timed_motor_port_start_v2(MOTOR_C, 100, 2000);
          //Cool down after kicking ball
          BT_drive(MOTOR_C, MOTOR_D, 30);
          BT_timed_motor_port_start_v2(MOTOR_C, 30, 100);
          ai->st.state = 107;
          
        }
        break;
    }
    case 107:
    {
        BT_all_stop(0);
        break;
    }
    case 201:
    {
        // Initialize Bot. 
        fprintf(stderr, "CHASE mode\n");
        BT_drive(MOTOR_C, MOTOR_D, 20);
        
        if(ai->st.ballID && ai->st.selfID){
            if(ai->st.svx == 0.0 && ai->st.svy == 0.0){ 
		fprintf(stderr, "Im wasnt moving this frame\n");
		BT_all_stop(1); 
		break;
            } else{
                fprintf(stderr, "-----------\nMy heading is: [%f, %f]\n-----------\n", ai->st.smx, ai->st.smy);
                double s = correct_heading(ai);
                new_dx = s * ai->st.self->dx;
                new_dy = s * ai->st.self->dy;
		        osmx = new_dx;
		        osmy = new_dy;
                fprintf(stderr, "Heading x = %f\n", osmx);
                fprintf(stderr, "Heading y = %f\n", osmy);
                fprintf(stderr, "Direction x = %f\n", new_dx);
                fprintf(stderr, "Direction y = %f\n", new_dy);
            }
	        fprintf(stderr, "Initialization is done\n");
            BT_all_stop(1);
            ai->st.state = 202;
        } else {
	      fprintf(stderr, "I couldnt find myself or the ball\n");
          BT_all_stop(1);
        }
        break;
    }
    case 202:
    {
        double dot_p = dot_product(new_dx, new_dy, osmx, osmy);
        double head = 1.0;
        fprintf(stderr, "X = %f, Y = %f\n", new_dx, new_dy);
        if(ai->st.ballID && ai->st.selfID){
           new_dx = ai->st.self->dx;
           new_dy = ai->st.self->dy;
           double b_to_self_x = ai->st.ball->cx - ai->st.self->cx;
           double b_to_self_y = ai->st.ball->cy - ai->st.self->cy;
           b_to_self_x = b_to_self_x / magnitude(b_to_self_x, b_to_self_y);
           b_to_self_y = b_to_self_y / magnitude(b_to_self_x, b_to_self_y);
           if(dot_product(new_dx, new_dy, osmx, osmy) < 0.0){
               new_dx = -1.0 * new_dx;
               new_dy = -1.0 * new_dy;
           }
           osmx = new_dx;
	       osmy = new_dy;
           //fprintf(stderr, "OSMX = %f\n", osmx);
           //fprintf(stderr, "OSMY = %f\n", osmy);
           //fprintf(stderr, "I want to align to: [%f, %f]|\n", b_to_self_x, b_to_self_y);
           //fprintf(stderr, "Cross Product: %f\n", cross_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
           //fprintf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
           if(cross_product(new_dx, new_dy, b_to_self_x, b_to_self_y) < 0.0){		
	         // Ball on left side. Rotate to left.
             //fprintf(stderr, "Ball on left side\n");
             if (dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y) <= 0.95) {
                 //Keep rotating.
                 //printf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
                 BT_turn(MOTOR_C, -40, MOTOR_D, 40);
                 
              } else {
                  fprintf(stderr, "Going to state 203");
                  ai->st.state = 203;
                  break;
                  
              }
           } else {
             // Ball on right side. 
             //fprintf(stderr, "Ball on right side\n");
             if (dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y) <= 0.95) {
                 //Keep rotating.
                 //fprintf(stderr, "Dot product of alignment = %f\n", dot_product(new_dx, new_dy, b_to_self_x, b_to_self_y));
                 BT_turn(MOTOR_C, 40, MOTOR_D, -40);
                 
              } else {
                  fprintf(stderr, "Going to state 203");
                  ai->st.state = 203;
                  break;
              }
           }           
           
        }
        
        break;
    }
    case 203:
    {
        BT_motor_port_start(MOTOR_B, 100);
        //Drive to Ball
        static double sx, sy;
        if(ai->st.ballID && ai->st.selfID){
            double dx, dy, m;
            dx = ai->st.ball->cx - ai->st.self->cx;
            dy = ai->st.ball->cx - ai->st.self->cx;
            m = magnitude(dx, dy);
            if(m >= 300.0){
                if(!sx && !sy){
                  sx = ai->st.smx;
                  sy = ai->st.smy;
                }
                else {
                  sx = 0.50 * ai->st.smx + 0.50 * ai->st.smx;
                  sy = 0.50 * ai->st.smy + 0.50 * ai->st.smy;
                }

            

            
            
                double mdx = dx / magnitude(dx, dy);
                double mdy = dy / magnitude(dx, dy);
                fprintf(stderr, "SX = %f\n", sx);
                fprintf(stderr, "SY = %f\n", sy);
                fprintf(stderr, "DX = %f\n", mdx);
                fprintf(stderr, "DY = %f\n", mdy);
                drive(sx, sy, mdx, mdy);
            } else {
               BT_all_stop(1);
               fprintf(stderr, "Going to state 204");
               ai->st.state = 204;
               break;
            }
            
        }
        break;
        /*if(ai->st.ballID && ai->st.selfID){
          //Find magnitude of bot to ball
          BT_motor_port_start(MOTOR_B, 100);
          double dist_to_ball = magnitude2(ai->st.self->cx, ai->st.self->cy, ai->st.ball->cx, ai->st.self->cy);
          fprintf(stderr, "Dist to ball: %f\n", dist_to_ball);
          if (dist_to_ball >= 350.0) {
             //Go to ball
             BT_drive(MOTOR_C, MOTOR_D, 100);
          } else {
             ai->st.state = 9;
          }
          break;
        } else {
          ai->st.state = 7;
        }*/

    }
    case 204:
    {
        if(ai->st.ballID && ai->st.selfID){
          // Distance to ball is close. Kick ball.
          BT_motor_port_start(MOTOR_B, 100);
          BT_drive(MOTOR_C, MOTOR_D, 100);
          BT_timed_motor_port_start_v2(MOTOR_C, 100, 500);
          //Cool down after kicking ball
          BT_drive(MOTOR_C, MOTOR_D, 30);
          BT_timed_motor_port_start_v2(MOTOR_C, 30, 100);
          ai->st.state = 202;
        }
        break;
    }
  }
 }

}

/**********************************************************************************
 TO DO:

 Add thdouble dot_product(double ax, double ay, double bx, double by)priate functions to
 handledouble dot_product(double ax, double ay, double bx, double by)ns in a meaningful
 way), double dot_product(double ax, double ay, double bx, double by)e space below.
filter_double dot_product(double ax, double ay, double bx, double by)
 AI_maidouble dot_product(double ax, double ay, double bx, double by)g. It should only call appropriate
 functidouble dot_product(double ax, double ay, double bx, double by)

 You widouble dot_product(double ax, double ay, double bx, double by)t doesn't belong
 there.double dot_product(double ax, double ay, double bx, double by)
**********************************************************************************/

 double compute_kick_position(RoboAI *ai){
     if(ai->st.ballID && ai->st.selfID){
        double kx = ai->st.ball->cx - (1.0 - ai->st.side)*1028;
        double ky = ai->st.ball->cy - (0.5 * 720);
        return ky * (ai->st.self->cx - (1.0 - ai->st.side)*1028) / kx + (0.5 * 720);
     }
     else return -1.0;
}

double magnitude2(double ax, double ay, double bx, double by) {
   double squared = pow(ax - bx , 2) + pow(ay - by, 2);
   return sqrt(squared);
}

double correct_heading(struct RoboAI *ai){
    double dot = (ai->st.smx * ai->st.self->dx) + (ai->st.smy * ai->st.self->dy);
    if(dot >= 0.0){
        return 1.0;
    }
    else return -1.0;
}
/*
    Generalized controller for drive in a straight line
    Need:
        - motion direction
        - angle to maintain w.r.t horizontal axis
    Goal:
        correct movement to align with specified angle
*/
// PID controlled drive foward and maintain an angle theta
void drive(double sx, double sy, double dx, double dy){
    double err = acos(dot_product(sx, sy, dx, dy));
    double Plf = 80.0 + 20.0 * err / (2.0 * PI);
    double Prf = 80.0 - 20.0 * err / (2.0 * PI);
    double Dlf = -1/sqrt(1 - pow(dot_product(sx, sy, dx, dy), 2));
    double Drf = 1/sqrt(1 - pow(dot_product(sx, sy, dx, dy), 2));
    fprintf(stderr, "Err = %f\n", err);
    double lf = Plf + Dlf;
    double rf = Prf + Drf;
    fprintf(stderr, "lf = %f\n", lf);
    fprintf(stderr, "rf = %f\n", rf);
    int l = (int) lf;
    int r = (int) rf;
    fprintf(stderr, "MOTOR L = %d\nMOTOR R = %d\n", l, r);
    BT_turn(MOTOR_C, lf, MOTOR_D, rf);
}
double magnitude(double x, double y){
    return pow((x * x + y * y), 0.5);
}

int detect_collision(RoboAI *ai){
    double dx = ai->st.self->cx - ai->st.opp->cx;
    double dy = ai->st.self->cy - ai->st.opp->cy;
    if(magnitude(dx, dy) <= 50.0){
        // Collision will occur soon, what's happening?
        double p = dot_product(ai->st.self->cx, ai->st.self->cy, ai->st.opp->cx, ai->st.opp->cy);
        if(p < -0.1){
            //Head on 
            BT_drive(MOTOR_C, MOTOR_D, -30);
            return 1.0;
        }
        else if(p >= -0.1 && p <= 0.1){
            //One of us is hitting the other from the side
            double t = (ai->st.opp->cx - ai->st.self->cx) / ai->st.smx;
            if(abs((ai->st.smy * t + ai->st.self->cy) - ai->st.opp->cy) <= 50.0){
                // We will soon be hitting the other bot from their side
		BT_all_stop(1);
                return 1.0;
            }
            else return 0.0;
        }
        else{
            // Bots are parallel
            double sx = ai->st.ball->cx - ai->st.self->cx;
            double sy = ai->st.ball->cy - ai->st.self->cy;
            double ox = ai->st.ball->cx - ai->st.opp->cx;
            double oy = ai->st.ball->cy - ai->st.opp->cy;
            if(magnitude(sx, sy) < magnitude(ox, oy)){
                //We are ahead in chasing the ball
                return 1; //?
            }
            else{
                //They are ahead, do something else
                return 1;
            }
        }
    }
    else return 0.0;
}

int detect_wall_collision(RoboAI *ai){
	if(ai->st.self->cy < 50.0){
        fprintf(stderr, "Am I hitting a wall? Dot = %f\n", dot_product(ai->st.smx, ai->st.smy, 0.0, 1.0));
	    if(dot_product(ai->st.smx, ai->st.smy, 0.0, 1.0) > 0.1){ 
            BT_drive(MOTOR_C, MOTOR_D, -30);
            return 1;
        }
	    else return 0;
	}
	else if(ai->st.self->cy > 670.0){
        fprintf(stderr, "Am I hitting a wall? Dot = %f\n", dot_product(ai->st.smx, ai->st.smy, 0.0, -1.0));
	    if(dot_product(ai->st.smx, ai->st.smy, 0.0, -1.0) > 0.1){ 
            BT_drive(MOTOR_C, MOTOR_D, -30);
            return 1;
        }
	    else return 0;
	}
	else if(ai->st.self->cx < 50.0){
        fprintf(stderr, "Am I hitting a wall? Dot = %f\n", dot_product(ai->st.smx, ai->st.smy, -1.0, 0.0));
	    if(dot_product(ai->st.smx, ai->st.smy, -1.0, 0.0) > 0.1){ 
            BT_drive(MOTOR_C, MOTOR_D, -30);
            return 1;
        }
	    else return 0;
	}
	else if(ai->st.self->cx > 970){
        fprintf(stderr, "Am I hitting a wall? Dot = %f\n", dot_product(ai->st.smx, ai->st.smy, 1.0, 0.0));
	    if(dot_product(ai->st.smx, ai->st.smy, 0.0, 1.0) > 0.1){ 
            BT_drive(MOTOR_C, MOTOR_D, -30);
            return 1;
        }
	    else return 0;
	}
    return 0;
}

double cross_product(double ax, double ay, double bx, double by){
    return (ax * by - ay * bx);              // Ball to the left
}
double dot_product(double ax, double ay, double bx, double by){
    return ax * bx + ay * by;
}
//Filter distance of sonar reading
int filter_sonar_dist(void) {
  static int prev_dist = 0;
  int read_dist = BT_read_ultrasonic_sensor(PORT_1);
  int curr_dist = 0;
  if (prev_dist != 0) {
    curr_dist = 0.5 * read_dist + (1 - 0.5) * prev_dist;
  } else {
    curr_dist = read_dist;
    prev_dist = read_dist;
  }
  return curr_dist;
}

