/*

  CSC C85 - Embedded Systems - Project # 1 - EV3 Robot Localization
  
 This file provides the implementation of all the functionality required for the EV3
 robot localization project. Please read through this file carefully, and note the
 sections where you must implement functionality for your bot. 
 
 You are allowed to change *any part of this file*, not only the sections marked
 ** TO DO **. You are also allowed to add functions as needed (which must also
 be added to the header file). However, *you must clearly document* where you 
 made changes so your work can be properly evaluated by the TA.

 NOTES on your implementation:

 * It should be free of unreasonable compiler warnings - if you choose to ignore
   a compiler warning, you must have a good reason for doing so and be ready to
   defend your rationale with your TA.
 * It must be free of memory management errors and memory leaks - you are expected
   to develop high wuality, clean code. Test your code extensively with valgrind,
   and make sure its memory management is clean.
 
 In a nutshell, the starter code provides:
 
 * Reading a map from an input image (in .ppm format). The map is bordered with red, 
   must have black streets with yellow intersections, and buildings must be either
   blue, green, or be left white (no building).
   
 * Setting up an array with map information which contains, for each intersection,
   the colours of the buildings around it in ** CLOCKWISE ** order from the top-left.
   
 * Initialization of the EV3 robot (opening a socket and setting up the communication
   between your laptop and your bot)
   
 What you must implement:
 
 * All aspects of robot control:
   - Finding and then following a street
   - Recognizing intersections
   - Scanning building colours around intersections
   - Detecting the map boundary and turning around or going back - the robot must not
     wander outside the map (though of course it's possible parts of the robot will
     leave the map while turning at the boundary)

 * The histogram-based localization algorithm that the robot will use to determine its
   location in the map - this is as discussed in lecture.

 * Basic robot exploration strategy so the robot can scan different intersections in
   a sequence that allows it to achieve reliable localization
   
 * Basic path planning - once the robot has found its location, it must drive toward a 
   user-specified position somewhere in the map.

 --- OPTIONALLY but strongly recommended ---
 
  The starter code provides a skeleton for implementing a sensor calibration routine,
 it is called when the code receives -1  -1 as target coordinates. The goal of this
 function should be to gather informatin about what the sensor reads for different
 colours under the particular map/room illumination/battery level conditions you are
 working on - it's entirely up to you how you want to do this, but note that careful
 calibration would make your work much easier, by allowing your robot to more
 robustly (and with fewer mistakes) interpret the sensor data into colours. 
 
   --> The code will exit after calibration without running localization (no target!)
       SO - your calibration code must *save* the calibration information into a
            file, and you have to add code to main() to read and use this
            calibration data yourselves.
   
 What you need to understand thoroughly in order to complete this project:
 
 * The histogram localization method as discussed in lecture. The general steps of
   probabilistic robot localization.

 * Sensors and signal management - your colour readings will be noisy and unreliable,
   you have to handle this smartly
   
 * Robot control with feedback - your robot does not perform exact motions, you can
   assume there will be error and drift, your code has to handle this.
   
 * The robot control API you will use to get your robot to move, and to acquire 
   sensor data. Please see the API directory and read through the header files and
   attached documentation
   
 Starter code:
 F. Estrada, 2018 - for CSC C85 
 
*/

#include "EV3_Localization.h"


static int ColorRefs[6][3];

int map[400][4];            // This holds the representation of the map, up to 20x20
                            // intersections, raster ordered, 4 building colours per
                            // intersection.
int sx, sy;                 // Size of the map (number of intersections along x and y)
double beliefs[400][4];     // Beliefs for each location and motion direction

/* Our constants */

int turn_time = 1000; // experimental, subject to change
int move_time = 1000; // experimental, subject to change
int AngleRef = 0; // Our ideal orientation relative to the current road, must be calibrated

int main(int argc, char *argv[])
{
 char mapname[1024];
 int dest_x, dest_y, rx, ry;
 unsigned char *map_image;
 
 memset(&map[0][0],0,400*4*sizeof(int));
 sx=0;
 sy=0;
 
 if (argc<4)
 {
  fprintf(stderr,"Usage: EV3_Localization map_name dest_x dest_y\n");
  fprintf(stderr,"    map_name - should correspond to a properly formatted .ppm map image\n");
  fprintf(stderr,"    dest_x, dest_y - target location for the bot within the map, -1 -1 calls calibration routine\n");
  exit(1);
 }
 strcpy(&mapname[0],argv[1]);
 dest_x=atoi(argv[2]);
 dest_y=atoi(argv[3]);
 if (dest_x == -2 && dest_y == -2){
   if (BT_open(HEXKEY)!=0){
    fprintf(stderr,"Unable to open comm socket to the EV3, make sure the EV3 kit is powered on, and that the\n");
    fprintf(stderr," hex key for the EV3 matches the one in EV3_Localization.h\n");
    exit(1);
   }
  
  int a,b,c,d;
  scan_intersection(&a, &b, &c, &d);
  BT_all_stop(1);
  
 }
 
 if (dest_x==-1&&dest_y==-1)
 {
  int c = 0;
  if (BT_open(HEXKEY)!=0){
    fprintf(stderr,"Unable to open comm socket to the EV3, make sure the EV3 kit is powered on, and that the\n");
    fprintf(stderr," hex key for the EV3 matches the one in EV3_Localization.h\n");
    exit(1);
  }
  
  calibrate_sensor();
  BT_close();
  exit(1);
 }

 /******************************************************************************************************************
  * OPTIONAL TO DO: If you added code for sensor calibration, add just below this comment block any code needed to
  *   read your calibration data for use in your localization code. Skip this if you are not using calibration
  * ****************************************************************************************************************/
 FILE *calibration = fopen("calibrations.txt", "r");
 if(calibration == NULL){
   fprintf(stderr, "Calibration Error: Cannot find calibrations file\n");
   exit(1);
 }
 

 int R, G, B = 0;
 int i = 0;
 while(fscanf(calibration, "%d %d %d\n", &R, &G, &B) != EOF) {
    fprintf(stderr, "%d %d %d\n", R, G, B);
    ColorRefs[i][0] = R;
    ColorRefs[i][1] = G;
    ColorRefs[i][2] = B;
    fprintf(stderr, "%d %d %d\n", ColorRefs[i][0], ColorRefs[i][1], ColorRefs[i][2]);
    if(i > 5){
        fprintf(stderr, "Calibration Error: more reference values than expected\n");
        exit(1);
    }
    i++;
 }
 for(int j=0; j < 6; j++) fprintf(stderr, "%d %d %d\n", ColorRefs[j][0], ColorRefs[j][1], ColorRefs[j][2]);
 if(i < 5){
    fprintf(stderr, "Calibration Error: less reference values than expected\n");
    exit(1);
 }
 i = 0;
 
 
 // Your code for reading any calibration information should not go below this line //
 
 map_image=readPPMimage(&mapname[0],&rx,&ry);
 if (map_image==NULL)
 {
  fprintf(stderr,"Unable to open specified map image\n");
  exit(1);
 }
 
 if (parse_map(map_image, rx, ry)==0)
 { 
  fprintf(stderr,"Unable to parse input image map. Make sure the image is properly formatted\n");
  free(map_image);
  exit(1);
 }

 if(dest_x==-10&&dest_y==-10);
 else if (dest_x<0||dest_x>=sx||dest_y<0||dest_y>=sy)
 {
  fprintf(stderr,"Destination location is outside of the map\n");
  free(map_image);
  exit(1);
 }

 // Initialize beliefs - uniform probability for each location and direction
 for (int j=0; j<sy; j++)
  for (int i=0; i<sx; i++)
  {
   beliefs[i+(j*sx)][0]=1.0/(double)(sx*sy*4);
   beliefs[i+(j*sx)][1]=1.0/(double)(sx*sy*4);
   beliefs[i+(j*sx)][2]=1.0/(double)(sx*sy*4);
   beliefs[i+(j*sx)][3]=1.0/(double)(sx*sy*4);
  }
 if(dest_x==-10&&dest_y==-10){
    int x,y,d;
    robot_localization(&x, &y, &d);
    exit(1);
 }
 // Open a socket to the EV3 for remote controlling the bot.
 if (BT_open(HEXKEY)!=0)
 {
  fprintf(stderr,"Unable to open comm socket to the EV3, make sure the EV3 kit is powered on, and that the\n");
  fprintf(stderr," hex key for the EV3 matches the one in EV3_Localization.h\n");
  free(map_image);
  exit(1);
 }
//fprintf(stderr, "\n\nAngleRef set to %d\n\n", AngleRef);
 fprintf(stderr,"All set, ready to go!\n");
 
/*******************************************************************************************************************************
 *
 *  TO DO - Implement the main localization loop, this loop will have the robot explore the map, scanning intersections and
 *          updating beliefs in the beliefs array until a single location/direction is determined to be the correct one.
 * 
 *          The beliefs array contains one row per intersection (recall that the number of intersections in the map_image
 *          is given by sx, sy, and that the map[][] array contains the colour indices of buildings around each intersection.
 *          Indexing into the map[][] and beliefs[][] arrays is by raster order, so for an intersection at i,j (with 0<=i<=sx-1
 *          and 0<=j<=sy-1), index=i+(j*sx)
 *  
 *          In the beliefs[][] array, you need to keep track of 4 values per intersection, these correspond to the belief the
 *          robot is at that specific intersection, moving in one of the 4 possible directions as follows:
 * 
 *          beliefs[i][0] <---- belief the robot is at intersection with index i, facing UP
 *          beliefs[i][1] <---- belief the robot is at intersection with index i, facing RIGHT
 *          beliefs[i][2] <---- belief the robot is at intersection with index i, facing DOWN
 *          beliefs[i][3] <---- belief the robot is at intersection with index i, facing LEFT
 * 
 *          Initially, all of these beliefs have uniform, equal probability. Your robot must scan intersections and update
 *          belief values based on agreement between what the robot sensed, and the colours in the map. 
 * 
 *          You have two main tasks these are organized into two major functions:
 * 
 *          robot_localization()    <---- Runs the localization loop until the robot's location is found
 *          go_to_target()          <---- After localization is achieved, takes the bot to the specified map location
 * 
 *          The target location, read from the command line, is left in dest_x, dest_y
 * 
 *          Here in main(), you have to call these two functions as appropriate. But keep in mind that it is always possible
 *          that even if your bot managed to find its location, it can become lost again while driving to the target
 *          location, or it may be the initial localization was wrong and the robot ends up in an unexpected place - 
 *          a very solid implementation should give your robot the ability to determine it's lost and needs to 
 *          run localization again.
 *
 *******************************************************************************************************************************/  

 // HERE - write code to call robot_localization() and go_to_target() as needed, any additional logic required to get the
 //        robot to complete its task should be here.

 //while(find_street());
 
 //timed_motor_port_start_v3(MOTOR_A, MOTOR_D, 30, 10);
 //int a = BT_timed_motor_port_start(MOTOR_A, 30, 1, 50000, 1);
 //fprintf(stderr, "I'm printing %d\n", a);
 //BT_timed_motor_port_start(MOTOR_A, 10, 5, 5000, 5);
 //BT_timed_motor_port_start(MOTOR_D, 10, 5, 5000, 5);
 //while(find_street());
 
 // Cleanup and exit - DO NOT WRITE ANY CODE BELOW THIS LINE
 
  while(drive_along_street());
  while(turn_left());
  while(drive_along_street());

 BT_close();
 free(map_image);
 
 exit(0);
}

int find_street(void)   
{
 /*
  * This function gets your robot onto a street, wherever it is placed on the map. You can do this in many ways, but think
  * about what is the most effective and reliable way to detect a street and stop your robot once it's on it.
  * 
  * You can use the return value to indicate success or failure, or to inform the rest of your code of the state of your
  * bot after calling this function
  */   
  int pl;
  int pr;
  // Found a street / already on a street
  // Check if we are aligned with the street
  int curr, forward, backward;
  curr = read_color();
    if(curr == BLACK || curr == YELLOW){
      BT_turn(MOTOR_A, -10, MOTOR_D, -10);
      BT_timed_motor_port_start_v2(MOTOR_D, -10, 500);
      BT_all_stop(1);
      forward = read_color();
      BT_turn(MOTOR_A, 10, MOTOR_D, 10);
      BT_timed_motor_port_start_v2(MOTOR_D, 10, 1000);
      BT_all_stop(1);
      backward = read_color();
      fprintf(stderr, "[READ ME] My 3 reading we're %d, %d, %d\n\n\n", curr, forward, backward);
      if(backward == BLACK || backward == YELLOW) backward = curr;
      if(forward == BLACK || forward == YELLOW) forward = curr;
      if(forward == curr && forward == backward){
        fprintf(stderr, "I'm aligned with a street!\n");
        AngleRef = read_avg_angle();
        sing();
        return 0;
      }
    }
    return 1;
for(int mi=0; mi < 400; mi++){
     fprintf(stderr, "\n\n Landmarks are [%d, %d, %d, %d]\n\n", map[mi][0], map[mi][1], map[mi][2], map[mi][3]);
 }
  
}


int drive_along_street(void)
{
 /*
  * This function drives your bot along a street, making sure it stays on the street without straying to other pars of
  * the map. It stops at an intersection.
  * 
  * You can implement this in many ways, including a controlled (PID for example), a neural network trained to track and
  * follow streets, or a carefully coded process of scanning and moving. It's up to you, feel free to consult your TA
  * or the course instructor for help carrying out your plan.
  * 
  * You can use the return value to indicate success or failure, or to inform the rest of your code of the state of your
  * bot after calling this function.
  */   

  //Motor power will be controlled by PID controller

  int static last_turn = 0;
  int curr_color = read_color();
  fprintf(stderr, "My curr color is %d\n", curr_color);
  int status = 1; //1 means keep going, 0 means stop
  fprintf(stderr, "Last turn was: %d\n", last_turn);
  if (curr_color == BLACK) {
    fprintf(stderr, "Driving on road \n");
    BT_drive(MOTOR_A, MOTOR_D, -10);
  } else if (curr_color == YELLOW) {
    BT_all_stop(1);
    return 0;
  } else if (curr_color == RED) {
    turn_around();
  } else {
    BT_drive(MOTOR_A, MOTOR_D, 7);
    while(curr_color != BLACK){
      curr_color = read_color();
    }
    BT_all_stop(1);
    int a = 100;
    while(1){
      BT_turn(MOTOR_A, 10, MOTOR_D, -10);
      BT_timed_motor_port_start_v2(MOTOR_A, 10, a);

      curr_color = read_color();
      if(curr_color == BLACK) break;
      BT_turn(MOTOR_A, -10, MOTOR_D, 10);
      BT_timed_motor_port_start_v2(MOTOR_D, 10, 2*a);
      curr_color = read_color();
      if(curr_color == BLACK) break;
      else a += 50;
    }
  }
  return 1;
}

int timed_turn_right(int time) {
  int i = 0;
  while (i < time) {
    BT_turn(MOTOR_A, 7, MOTOR_D, -7);
    i++;
  } 
  BT_all_stop(0);
  return 1;
}

int timed_turn_left(int time) {
  int i = 0;
  while (i < time) {
    BT_turn(MOTOR_A, -7, MOTOR_D, 7);
    i++;
  } 
  BT_all_stop(0);
  return 1;
}


int scan_intersection(int *tl, int *tr, int *br, int *bl)
{
  /* This function carries out the intersection scan - the bot should (obviously) be placed at an intersection for this,
  * and the specific set of actions will depend on how you designed your bot and its sensor. Whatever the process, you
  * should make sure the intersection scan is reliable - i.e. the positioning of the sensor is reliably over the buildings
  * it needs to read, repeatably, and as the robot moves over the map.
  * 
  * Use the APIs sensor reading calls to poll the sensors. You need to remember that sensor readings are noisy and 
  * unreliable so * YOU HAVE TO IMPLEMENT SOME KIND OF SENSOR / SIGNAL MANAGEMENT * to obtain reliable measurements.
  * 
  * Recall your lectures on sensor and noise management, and implement a strategy that makes sense. Document your process
  * in the code below so your TA can quickly understand how it works.
  * 
  * Once your bot has read the colours at the intersection, it must return them using the provided pointers to 4 integer
  * variables:
  * 
  * tl - top left building colour
  * tr - top right building colour
  * br - bottom right building colour
  * bl - bottom left building colour
  * 
  * The function's return value can be used to indicate success or failure, or to notify your code of the bot's state
  * after this call.
  */
 
  /************************************************************************************************************************
   *   TO DO  -   Complete this function
   */

 // Return invalid colour values, and a zero to indicate failure (you will replace this with your code)
 
 //Boolean values to check if all the intersection has been scanned

 int check_step = 0;
 int curr_color = read_color();

 fprintf(stderr, "My current color is: %d\n", curr_color);
 fprintf(stderr, "Checking tl status: %d\n", check_step);

 //Scan top left
 if (check_step == 0) {
   if (curr_color == BLACK || curr_color == YELLOW) {
     //Turn until the color sensor finds a building
     BT_turn(MOTOR_A, -10, MOTOR_D, 0); 
   } else {
     BT_all_stop(0);
     //Record top left color
     *(tl) = curr_color;
     fprintf(stderr, "Building at tl is: %d\n", *(tl));
   }
   
   //Return back to original position
 
   if (curr_color != YELLOW) {
     //Turn until the color sensor finds a building
     BT_turn(MOTOR_A, 10, MOTOR_D, 0); 
   } else {
     BT_all_stop(0);
 
     fprintf(stderr, "Building at br is: %d\n", *(br));
   }
 int check_step = 0;
   //Return back to original position
 
   if (curr_color != YELLOW) {
 fprintf(stderr, "Checking tl status: %d\n", check_step);
     BT_turn(MOTOR_A, -10, MOTOR_D, 0); 
   } else {
 if (check_step == 0) {
   }

   //Check if sensor is back at original position
   if (*(br) != 0 && curr_color == YELLOW) {
     //Mark corner as checked
     check_step += 1;
   }
 }
 //Scan bottom right


 return(1);
 
}
}
int turn_around(void){
   int curr_color = read_color();
   //Perform 180 turn
   if (curr_color != BLACK && (curr_color == RED || curr_color == WHITE || curr_color == BLUE || curr_color == GREEN)) {
     BT_turn(MOTOR_A, -15, MOTOR_D, 15);
   } 
   BT_all_stop(0);
  
}

int turn_at_intersection(int turn_direction){
 /*
  * This function is used to have the robot turn either left or right at an intersection (obviously your bot can not just
  * drive forward!). 
  * 
  * If turn_direction=0, turn right, else if turn_direction=1, turn left.
  * 
  * You're free to implement this in any way you like, but it should reliably leave your bot facing the correct direction
  * and on a street it can follow. 
  * 
  * You can use the return value to indicate success or failure, or to inform your code of the state of the bot
  */
  

  return(1);
}

int robot_localization(int *robot_x, int *robot_y, int *direction)
{
 /*  This function implements the main robot localization process. You have to write all code that will control the robot
  *  and get it to carry out the actions required to achieve localization.
  *
  *  Localization process:
  *
  *  - Find the street, and drive along the street toward an intersection
  *  - Scan the colours of buildings around the intersection
     check_step += 1;
  *  - Repeat the process until a single intersection/facing direction is distintly more likely than all the rest
 } else if (check_step == 3) {
  *  * We have provided headers for the following functions:
  * 
  *  find_street()
  *  drive_along_street()
  *  scan_intersection()
  *  turn_at_intersection()
  * 
  *  You *do not* have to use them, and can write your own to organize your robot's work as you like, they are
  *  provided as a suggestion.
  * 
  *  Note that *your bot must explore* the map to achieve reliable localization, this means your intersection
  *  scanning strategy should not rely exclusively on moving forward, but should include turning and exploring
  *  other streets than the one your bot was initially placed on.
  * 
  *  For each of the control functions, however, you will need to use the EV3 API, so be sure to become familiar with
  *  it.
  * 
  *  In terms of sensor management - the API allows you to read colours either as indexed values or RGB, it's up to
  *  you which one to use, and how to interpret the noisy, unreliable data you're likely to get from the sensor
  *  in order to update beliefs.
  * 
  *  HOWEVER: *** YOU must document clearly both in comments within this function, and in your report, how the
     check_step += 1;
  * 
  *  DO NOT FORGET - Beliefs should always remain normalized to be a probability distribution, that means the
  *                  sum of beliefs over all intersections and facing directions must be 1 at all times.
  * 
  *  The function receives as input pointers to three integer values, these will be used to store the estimated
  *   robot's location and facing direction. The direction is specified as:
  *   1 - RIGHT
  *   2 - DOWN
  *   3 - LEFT
  *  The function's return value is 1 if localization was successful, and 0 otherwise.
  */
 
  /************************************************************************************************************************
   *   TO DO  -   Complete this function
   ***********************************************************************************************************************/

 // Return an invalid location/direction and notify that localization was unsuccessful (you will delete this and replace it
 // with your code).
 *(robot_x)=-1;
 *(robot_y)=-1;
 *(direction)=-1;
 int tl, tr, bl, br;
 double threshold = 0.8;
 /*
 while(find_street());
 while(drive_along_street());
 turn_at_intersection(1);
 while(find_street());
 while(drive_along_street());
 scan_intersection(&tl, &tr, &br, &bl);
 updateBeliefs(tl, tr, br, bl, 3);
 */
 
 updateBeliefs(3, 6, 2, 6, -1);
 fprintf(stderr, "My best belief is %f\n", strongest_belief());
 updateBeliefs(2, 2, 6, 3, 2);
 fprintf(stderr, "My best belief is %f\n", strongest_belief());
 updateBeliefs(6, 2, 6, 3, 3);
 fprintf(stderr, "My best belief is %f\n", strongest_belief());
 updateBeliefs(6, 3, 6, 3, 3);
 fprintf(stderr, "My best belief is %f\n", strongest_belief());
 updateBeliefs(3, 6, 2, 6, 0);
 fprintf(stderr, "My best belief is %f\n", strongest_belief());
 //updateBeliefs(3, 2, 2, 6, 1);
 double b = strongest_belief();
 fprintf(stderr, "My best belief is %f\n", b);
 for(int i; i < (sx * sy); i++){
   for(int j=0; j<4; j++){
    if(beliefs[i][j] == b) fprintf(stderr, "My best bet is at intersection %d with direction %d\n", i, j);
    if(b >= threshold){
      *(robot_x) = i % sx;
      *(robot_y) = i;
      *(direction) = j;
      return(0);
    }
   }
 }
 return(1);
}

int go_to_target(int robot_x, int robot_y, int direction, int target_x, int target_y)
{
 /*
  * This function is called once localization has been successful, it performs the actions required to take the robot
  * from its current location to the specified target location. 
  *
  * You have to write the code required to carry out this task - once again, you can use the function headers provided, or
  * write your own code to control the bot, but document your process carefully in the comments below so your TA can easily
  * understand how everything works.
  *
  * Your code should be able to determine if the robot has gotten lost (or if localization was incorrect), and your bot
  * should be able to recover.
  * 
  * Inputs - The robot's current location x,y (the intersection coordinates, not image pixel coordinates)
  *          The target's intersection location
  * 
  * Return values: 1 if successful (the bot reached its target destination), 0 otherwise
  */   

  /************************************************************************************************************************
   *   TO DO  -   Complete this function
   ***********************************************************************************************************************/
  if(robot_x == target_x && robot_y == target_y){
    // at destination, goal complete
    // halt robot here or do the work in another function?
    return(1);
  }

  return(0);  
}

void calibrate_sensor(void)
{
 /*
  * This function is called when the program is started with -1  -1 for the target location. 
  *
  * You DO NOT NEED TO IMPLEMENT ANYTHING HERE - but it is strongly recommended as good calibration will make sensor
  * readings more reliable and will make your code more resistent to changes in illumination, map quality, or battery
  * level.
  * 
  * The principle is - Your code should allow you to sample the different colours in the map, and store representative
  * values that will help you figure out what colours the sensor is reading given the current conditions.
  * 
  * Inputs - None
  * Return values - None - your code has to save the calibration information to a file, for later use (see in main())
  * 
  * How to do this part is up to you, but feel free to talk with your TA and instructor about it!
  */   

  /************************************************************************************************************************
   *   OPTIONAL TO DO  -   Complete this function
   ***********************************************************************************************************************/
  fprintf(stderr,"Calibration function called!\n");  
  char resp[10];
  int avg[3];
  int refAngle = read_avg_angle();
  FILE *out_file = fopen("calibrations.txt", "w");
  FILE *ref_file = fopen("orientation.txt", "w");
  /*
  Order:
    red
    white
    black
    blue
    green
    yellow
  */
  fprintf(stderr, "Reading color for Black\n");
  read_avg_color(avg);

  fprintf(stderr, "Avg color reading is [%d, %d, %d]\n", avg[0], avg[1], avg[2]);
  fprintf(stderr, "Press enter to calibrate sensor for Blue\n");
  fprintf(out_file, "%d %d %d\n", avg[0], avg[1], avg[2]);
  fgets(resp, 10, stdin);
  read_avg_color(avg);

  fprintf(stderr, "Avg color reading is [%d, %d, %d]\n", avg[0], avg[1], avg[2]);
  fprintf(stderr, "Press enter to calibrate sensor for Green\n");
  fprintf(out_file, "%d %d %d\n", avg[0], avg[1], avg[2]);
  fgets(resp, 10, stdin);
  read_avg_color(avg);

  fprintf(stderr, "Avg color reading is [%d, %d, %d]\n", avg[0], avg[1], avg[2]);
  fprintf(stderr, "Press enter to calibrate sensor for Yellow\n");
  fprintf(out_file, "%d %d %d\n", avg[0], avg[1], avg[2]);
  fgets(resp, 10, stdin);
  read_avg_color(avg);

  fprintf(stderr, "Avg color reading is [%d, %d, %d]\n", avg[0], avg[1], avg[2]);
  fprintf(stderr, "Press enter to calibrate sensor for Red\n");
  fprintf(out_file, "%d %d %d\n", avg[0], avg[1], avg[2]);
  fgets(resp, 10, stdin);
  read_avg_color(avg);

  fprintf(stderr, "Avg color reading is [%d, %d, %d]\n", avg[0], avg[1], avg[2]);
  fprintf(stderr, "Press enter to calibrate sensor for White\n");
  fprintf(out_file, "%d %d %d\n", avg[0], avg[1], avg[2]);
  fgets(resp, 10, stdin);
  read_avg_color(avg);
  
  fprintf(stderr, "Avg color reading is [%d, %d, %d]\n", avg[0], avg[1], avg[2]);
  fprintf(out_file, "%d %d %d\n", avg[0], avg[1], avg[2]);
  
}

int parse_map(unsigned char *map_img, int rx, int ry)
{
 /*
   This function takes an input image map array, and two integers that specify the image size.
   It attempts to parse this image into a representation of the map in the image. The size
   and resolution of the map image should not affect the parsing (i.e. you can make your own
   maps without worrying about the exact position of intersections, roads, buildings, etc.).

   However, this function requires:
   
   * White background for the image  [255 255 255]
   * Red borders around the map  [255 0 0]
   * Black roads  [0 0 0]
   * Yellow intersections  [255 255 0]
   * Buildings that are pure green [0 255 0], pure blue [0 0 255], or white [255 255 255]
   (any other colour values are ignored - so you can add markings if you like, those 
    will not affect parsing)

   The image must be a properly formated .ppm image, see readPPMimage below for details of
   the format. The GIMP image editor saves properly formatte/*
    ----- TO DO -----
  > write a updateBeliefs func to update beliefs given zk, x(k-1), and a
  > determine threshold to complete localization

*
 // Scan intersection and find the 4 corners
 
 d .ppm images, as does the
   imagemagick image processing suite./*
    ----- TO DO -----
  > write a updateBeliefs func to update beliefs given zk, x(k-1), and a
  > determine threshold to complete localization

*
 // Scan intersection and find the 4 corners
 
 
   
   The map representation is read into the map array, with each row in the array corrsponding
   to one intersection, in raster order, that is, for a map with k intersections along its width:
   
    (row index for the intersection)
    
    0     1     2    3 ......   k-1
    
    k    k+1   k+2  ........    
    
    Each row will then contain the colour values for buildings around the intersection 
    clockwise from top-left, that is
    
    
    top-left               top-right
            
            intersection
    
    bottom-left           bottom-right
    
    So, for the first intersection (at row 0 in the map array)
    map[0][0] <---- colour for the top-left building
    map[0][1] <---- colour for the top-right building
    map[0][2] <---- colour for the bottom-right building
    map[0][3] <---- colour for the bottom-left building
    
    Color values for map locations are defined as follows (this agrees with what the
    EV3 sensor returns in indexed-colour-reading mode):
    
    1 -  Black
    2 -  Blue
    3 -  Green
    4 -  Yellow
    5 -  Red
    6 -  White
    
    If you find a 0, that means you're trying to access an intersection that is not on the
    map! Also note that in practice, because of how the map is defined, you should find
    only Green, Blue, or White around a given intersection.
    
    The map size (the number of intersections along the horizontal and vertical directions) is
    updated and left in the global variables sx and sy.

    Feel free to create your own maps for testing (you'll have to print them to a reasonable
    size to use with your bot).
    
 */    
 
 int last3[3];
 int x,y;
 unsigned char R,G,B;
 int ix,iy;
 int bx,by,dx,dy,wx,wy;         // Intersection geometry parameters
 int tgl;
 int idx;
 
 ix=iy=0;       // Index to identify the current intersection
 
 // Determine the spacing and size of intersections in the map
 tgl=0;
 for (int i=0; i<rx; i++)
 {
  for (int j=0; j<ry; j++)
  {
   R=*(map_img+((i+(j*rx))*3));
   G=*(map_img+((i+(j*rx))*3)+1);
   B=*(map_img+((i+(j*rx))*3)+2);
   if (R==255&&G==255&&B==0)
   {
    // First intersection, top-left pixel. Scan right to find width and spacing
    bx=i;           // Anchor for intersection locations
    by=j;
    for (int k=i; k<rx; k++)        // Find width and horizontal distance to next intersection
    {
     R=*(map_img+((k+(by*rx))*3));
     G=*(map_img+((k+(by*rx))*3)+1);
     B=*(map_img+((k+(by*rx))*3)+2);
     if (tgl==0&&(R!=255||G!=255||B!=0))
     {
      tgl=1;
      wx=k-i;
     }
     if (tgl==1&&R==255&&G==255&&B==0)
     {
      tgl=2;
      dx=k-i;
     }
    }
    for (int k=j; k<ry; k++)        // Find height and vertical distance to next intersection
    {
     R=*(map_img+((bx+(k*rx))*3));
     G=*(map_img+((bx+(k*rx))*3)+1);
     B=*(map_img+((bx+(k*rx))*3)+2);
     if (tgl==2&&(R!=255||G!=255||B!=0))
     {
      tgl=3;
      wy=k-j;
     }
     if (tgl==3&&R==255&&G==255&&B==0)
     {
      tgl=4;
      dy=k-j;
     }
    }
    
    if (tgl!=4)
    {
     fprintf(stderr,"Unable to determine intersection geometry!\n");
     return(0);
    }
    else break;
   }
  }
  if (tgl==4) break;
 }
  fprintf(stderr,"Intersection parameters: base_x=%d, base_y=%d, width=%d, height=%d, horiz_distance=%d, vertical_distance=%d\n",bx,by,wx,wy,dx,dy);

  sx=0;
  for (int i=bx+(wx/2);i<rx;i+=dx)
  {
   R=*(map_img+((i+(by*rx))*3));
   G=*(map_img+((i+(by*rx))*3)+1);
   B=*(map_img+((i+(by*rx))*3)+2);
   if (R==255&&G==255&&B==0) sx++;
  }

  sy=0;
  for (int j=by+(wy/2);j<ry;j+=dy)
  {
   R=*(map_img+((bx+(j*rx))*3));
   G=*(map_img+((bx+(j*rx))*3)+1);
   B=*(map_img+((bx+(j*rx))*3)+2);
   if (R==255&&G==255&&B==0) sy++;
  }
  
  fprintf(stderr,"Map size: Number of horizontal intersections=%d, number of vertical intersections=%d\n",sx,sy);

  // Scan for building colours around each intersection
  idx=0;
  for (int j=0; j<sy; j++)
   for (int i=0; i<sx; i++)
   {
    x=bx+(i*dx)+(wx/2);
    y=by+(j*dy)+(wy/2);
    
    fprintf(stderr,"Intersection location: %d, %d\n",x,y);
    // Top-left
    x-=wx;
    y-=wy;
    R=*(map_img+((x+(y*rx))*3));
    G=*(map_img+((x+(y*rx))*3)+1);
    B=*(map_img+((x+(y*rx))*3)+2);
    if (R==0&&G==255&&B==0) map[idx][0]=3;
    else if (R==0&&G==0&&B==255) map[idx][0]=2;
    else if (R==255&&G==255&&B==255) map[idx][0]=6;
    else fprintf(stderr,"Colour is not valid for intersection %d,%d, Top-Left RGB=%d,%d,%d\n",i,j,R,G,B);

    // Top-right
    x+=2*wx;
    R=*(map_img+((x+(y*rx))*3));
    G=*(map_img+((x+(y*rx))*3)+1);
    B=*(map_img+((x+(y*rx))*3)+2);
    if (R==0&&G==255&&B==0) map[idx][1]=3;
    else if (R==0&&G==0&&B==255) map[idx][1]=2;
    else if (R==255&&G==255&&B==255) map[idx][1]=6;
    else fprintf(stderr,"Colour is not valid for intersection %d,%d, Top-Right RGB=%d,%d,%d\n",i,j,R,G,B);

    // Bottom-right
    y+=2*wy;
    R=*(map_img+((x+(y*rx))*3));
    G=*(map_img+((x+(y*rx))*3)+1);
    B=*(map_img+((x+(y*rx))*3)+2);
    if (R==0&&G==255&&B==0) map[idx][2]=3;
    else if (R==0&&G==0&&B==255) map[idx][2]=2;
    else if (R==255&&G==255&&B==255) map[idx][2]=6;
    else fprintf(stderr,"Colour is not valid for intersection %d,%d, Bottom-Right RGB=%d,%d,%d\n",i,j,R,G,B);
    
    // Bottom-left
    x-=2*wx;
    R=*(map_img+((x+(y*rx))*3));
    G=*(map_img+((x+(y*rx))*3)+1);
    B=*(map_img+((x+(y*rx))*3)+2);
    if (R==0&&G==255&&B==0) map[idx][3]=3;
    else if (R==0&&G==0&&B==255) map[idx][3]=2;
    else if (R==255&&G==255&&B==255) map[idx][3]=6;
    else fprintf(stderr,"Colour is not valid for intersection %d,%d, Bottom-Left RGB=%d,%d,%d\n",i,j,R,G,B);
    
    fprintf(stderr,"Colours for this intersection: %d, %d, %d, %d\n",map[idx][0],map[idx][1],map[idx][2],map[idx][3]);
    
    idx++;
   }

 return(1);  
}

unsigned char *readPPMimage(const char *filename, int *rx, int *ry)
{
 // Reads an image from a .ppm file. A .ppm file is a very simple image representation
 // format with a text header followed by the binary RGB data at 24bits per pixel.
 // The header has the following form:
 //
 // P6
 // # One or more comment lines preceded by '#'
 // 340 200
 // 255
 //
 // The first line 'P6' is the .ppm format identifier, this is followed by one or more
 // lines with comments, typically used to inidicate which program generated the
 // .ppm file.
 // After the comments, a line with two integer values specifies the image resolution
 // as number of pixels in x and number of pixels in y.
 // The final line of the header stores the maximum value for pixels in the image,
 // usually 255.
 // After this last header line, binary data stores the RGB values for each pixel
 // in row-major order. Each pixel requires 3 bytes ordered R, G, and B.
 //
 // NOTE: Windows file handling is rather crotchetty. You may have to change the
 //       way this file is accessed if the images are being corrupted on read
 //       on Windows.
 //

 FILE *f;
 unsigned char *im;
 char line[1024]; //d .ppm images, as does the
   //imagemagick image processing suite.
 int i;
 unsigned char *tmp;
 double *fRGB;

 im=NULL;
 f=fopen(filename,"rb+");
 if (f==NULL)
 {
  fprintf(stderr,"Unable to open file %s for reading, please check name and path\n",filename);
  return(NULL);
 }
 fgets(&line[0],1000,f);
 if (strcmp(&line[0],"P6\n")!=0)
 {
  fprintf(stderr,"Wrong file format, not a .ppm file or header end-of-line characters missing\n");
  fclose(f);
  return(NULL);
 }
 fprintf(stderr,"%s\n",line);
 // Skip over comments
 fgets(&line[0],511,f);
 while (line[0]=='#')
 {
  fprintf(stderr,"%s",line);
  fgets(&line[0],511,f);
 }
 sscanf(&line[0],"%d %d\n",rx,ry);                  // Read image size
 fprintf(stderr,"nx=%d, ny=%d\n\n",*rx,*ry);

 fgets(&line[0],9,f);  	                // Read the remaining header line
 fprintf(stderr,"%s\n",line);
 im=(unsigned char *)calloc((*rx)*(*ry)*3,sizeof(unsigned char));
 if (im==NULL)
 {
  fprintf(stderr,"Out of memory allocating space for image\n");
  fclose(f);
  return(NULL);
 }
 fread(im,(*rx)*(*ry)*3*sizeof(unsigned char),1,f);
 fclose(f);

 return(im);    
}

/*
	Helper and test functions
	Can be found below
*/

int move_forward(char pwr){
  /*
	Move the robot forward
	returns 1 if error occurs, 0 otherwise

	Aside: pwr coefficient might be unneeded
  */
  BT_drive(MOTOR_A, MOTOR_D, pwr);
  return 0;
}
int turn_left(){
  int curr_color = read_color();
   if (curr_color != BLACK) {
     BT_turn(MOTOR_A, -15, MOTOR_D, 10);
     return 1;
   } 
  return 0;
}

int turn_right(){
  /*
  	Turn the robot 90 degrees to the right
	returns 1 if error occurs, 0 otherwise
  */
   int curr_color = read_color();
   if (curr_color == YELLOW || curr_color == BLACK) {
     BT_turn(MOTOR_A, 10, MOTOR_D, -15);
   }
  return 1;
}

int read_avg_color(int *RGB){
    //Return the average colour out of 20 readings
    int curr[3];
    RGB[0] = 0;
    RGB[1] = 0;
    RGB[2] = 0;
    for(int i=0; i < 9; i++){
      fprintf(stderr, "I am returning [%d, %d, %d]\n", curr[0], curr[1], curr[2]);
      BT_read_colour_sensor_RGB(PORT_1, curr);
      if(curr[0] < 0 || curr[1] < 0 || curr[2] < 0) return 1;
      RGB[0] = RGB[0] + min(curr[0], 255);
      RGB[1] = RGB[1] + min(curr[1], 255);
      RGB[2] = RGB[2] + min(curr[2], 255);
    }
    RGB[0] = RGB[0]/10;
    RGB[1] = RGB[1]/10;
    RGB[2] = RGB[2]/10;
    
    
    return 0;
}

int read_avg_angle(){
    int avg = 0;
    for(int i=0; i < 19; i++){
        int curr = BT_read_gyro_sensor(PORT_4);
        avg = avg + curr;
    }
    avg = avg/20;
    return avg % 360;
}

int read_color(){
  /*
	Take the reading from the sensor and compute it into
	and meaningful reading
  */
  int RGB[3];
  int ref = 0;
  int err = 195075;
  BT_read_colour_sensor_RGB(PORT_1, RGB);
  for(int i=0; i < 6; i++){
    int eval = pow(RGB[0] - ColorRefs[i][0], 2) + pow(RGB[1] - ColorRefs[i][1], 2) + pow(RGB[2] - ColorRefs[i][2], 2);
    if(eval < err) {
        err = eval;
        ref = i + 1;
    }
  }
  fprintf(stderr, "My best guess is %d\n", ref);
  // something went wrong / not a valid reading (in terms on the map that is)
  return ref;
}

int min(int a, int b){
    if(a > b) return b;
    else return a;
}

int read_angle(){
  return 0;
}

void sing(){
  int tone_data[50][3];
 
  // Reset tone data information
  for (int i=0;i<50; i++) 
  {
    tone_data[i][0]=-1;
    tone_data[i][1]=-1;
    tone_data[i][2]=-1;
  }
 
  tone_data[0][0]=262;
  tone_data[0][1]=250;
  tone_data[0][2]=1;
  tone_data[1][0]=330;
  tone_data[1][1]=250;
  tone_data[1][2]=25;
  tone_data[2][0]=392;
  tone_data[2][1]=250;
  tone_data[2][2]=50;
  tone_data[3][0]=523;
  tone_data[3][1]=250;
  tone_data[3][2]=63;
  BT_play_tone_sequence(tone_data);
}

void updateBeliefs(int tl, int tr, int br, int bl, int action){
  /*
    Update beliefs given intersection reading, and action taken

    action =: (0) UP
              (1) RIGHT
              (2) DOWN
              (3) LEFT
              (-1) Unitialized
  */
 for(int i=0; i<400; i++){
   double pz = 0.05;
   double pu = 0.3;
   double pl = 0.3;
   double pr = 0.3;
   double pd = 0.3;
   if(action == -1){
     pu = 0.05;
     pd = 0.05;
     pl = 0.05;
     pr = 0.05;
   }
   if(map[i][0] != 0){
     if(tl == map[i][0] && tr == map[i][1] && bl == map[i][3] && br == map[i][2]){
       //Update belief that bot is on intersection i with direction UP (0)
       pz = 0.95;
       if(action == 0) pu = .7;
       else if (action == -1) pu = 0.95;
     } 
     else if(tl == map[i][1] && tr == map[i][2] && bl == map[i][0] && br == map[i][3]){
       //Update belief that bot is on intersection i with direction RIGHT (1)
       pz = 0.95;
       if(action == 1)pr = .7;
       else if (action == -1) pr = 0.95;
     }
     else if(tl == map[i][2] && tr == map[i][3] && bl == map[i][1] && br == map[i][0]){
       //Update belief that bot is on intersection i with direction DOWN (2)
       pz = 0.25 * 0.95;
       if(action == 2) pd = .7;
       else if (action == -1) pd = 0.95;
     } 
     else if(tl == map[i][3] && tr == map[i][0] && bl == map[i][2] && br == map[i][1]){
       //Update belief that bot is on intersection i with direction LEFT (3)
       pz = 0.25 * 0.95;
       if(action == 3) pl = .7;
       else if (action == -1) pl = 0.95;
     }
     
     beliefs[i][0] = pz * pu * beliefs[i][0];
     beliefs[i][1] = pz * pr * beliefs[i][1];
     beliefs[i][2] = pz * pd * beliefs[i][2];
     beliefs[i][3] = pz * pl * beliefs[i][3];
   }
 }
 // Re-normalize
 double pwd = 0.0;
 for(int j=0; j<400; j++){
   for(int k=0; k<4; k++){
     pwd += beliefs[j][k];
   }
 }
 for(int l=0; l<(sx * sy); l++){
   for(int m=0; m<4; m++){
     beliefs[l][m] = (beliefs[l][m] / pwd);
   }
 }
}

void interactive_localization(){
    // Dont use, fgets is not reading stdin correctly
    fprintf(stderr, "Welcome to interactive localization\nPlease enter the the 4 colours scanned at the intersection\n");
    /*while(1){
      int c1, c2, c3, c4;
  return 0;
      scanf("%d *[^\n]", &br);
      //while ((c3 = getchar()) != '\n' && c3 != EOF);
      fprintf(stderr, "Lastly enter colour scanned at the bottom left corner\n");
      scanf("%d *[^\n]", &bl);
      //while ((c4 = getchar()) != '\n' && c4 != EOF);
      fprintf(stderr, "For confirmation, your scanned lands marks are [%d, %d, %d, %d]\n", tl, tr, br, bl);
      fprintf(stderr, "Please provide an action:\n\tMove UP (0)\n\tMove Right (1)\n\tMove DOWN (2)\n\tMove LEFT (3)\n");
      scanf("%d *[^\n]", &a);
      //while ((c1 = getchar()) != '\n' && c1 != EOF);
      fprintf(stderr, "Updating beliefs given intersection and movement of %d\n", a);
      if(a > 3 || a < 0) break;
      fprintf(stderr, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
    }*/
    
    fprintf(stderr, "Interactive Localization Ending\n");
}

double strongest_belief(){
  double best = 0.0;
  for(int i=0; i<400; i++){
    for(int j=0; j<4; j++){
      if(beliefs[i][j] > best) best = beliefs[i][j];
    }
  }
  return best;
}
