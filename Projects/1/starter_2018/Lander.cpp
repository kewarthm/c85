/*
	Lander Control simulation.

	Updated by F. Estrada for CSC C85, Oct. 2013
	Updated by Per Parker, Sep. 2015

	Learning goals:

	- To explore the implementation of control software
	  that is robust to malfunctions/failures.

	The exercise:

	- The program loads a terrain map from a .ppm file.
	  the map shows a red platform which is the location
	  a landing module should arrive at.
	- The control software has to navigate the lander
	  to this location and deposit the lander on the
	  ground considering:

	  * Maximum vertical speed should be less than 5 m/s at touchdown
	  * Maximum landing angle should be less than 10 degrees w.r.t vertical

	- Of course, touching any part of the terrain except
	  for the landing platform will result in destruction
	  of the lander

	This has been made into many videogames. The oldest one
	I know of being a C64 game called 1985 The Day After.
        There are older ones! (for bonus credit, find the oldest
        one and send me a description/picture plus info about the
        platform it ran on!)

	Your task:

	- These are the 'sensors' you have available to control
          the lander.

	  Velocity_X();  - Gives you the lander's horizontal velocity
	  Velocity_Y();	 - Gives you the lander's vertical velocity
	  Position_X();  - Gives you the lander's horizontal position (0 to 1024)
	  Position Y();  - Gives you the lander's vertical position (0 to 1024)

          Angle();	 - Gives the lander's angle w.r.t. vertical in DEGREES (upside-down = 180 degrees)

	  SONAR_DIST[];  - Array with distances obtained by sonar. Index corresponds
                           to angle w.r.t. vertical direction measured clockwise, so that
                           SONAR_DIST[0] is distance at 0 degrees (pointing upward)
                           SONAR_DIST[1] is distance at 10 degrees from vertical
                           SONAR_DIST[2] is distance at 20 degrees from vertical
                           .
                           .
                           .
                           SONAR_DIST[35] is distance at 350 degrees from vertical

                           if distance is '-1' there is no valid reading. Note that updating
                           the sonar readings takes time! Readings remain constant between
                           sonar updates.

          RangeDist();   - Uses a laser range-finder to accurately measure the distance to ground
                           in the direction of the lander's main thruster.
                           The laser range finder never fails (probably was designed and
                           built by PacoNetics Inc.)

          Note: All sensors are NOISY. This makes your life more interesting.

	- Variables accessible to your 'in flight' computer

	  MT_OK		- Boolean, if 1 indicates the main thruster is working properly
	  RT_OK		- Boolean, if 1 indicates the right thruster is working properly
	  LT_OK		- Boolean, if 1 indicates thr left thruster is working properly
          PLAT_X	- X position of the landing platform
          PLAY_Y        - Y position of the landing platform

	- Control of the lander is via the following functions
          (which are noisy!)

	  Main_Thruster(double power);   - Sets main thurster power in [0 1], 0 is off
	  Left_Thruster(double power);	 - Sets left thruster power in [0 1]
	  Right_Thruster(double power);  - Sets right thruster power in [0 1]
	  Rotate(double angle);	 	 - Rotates module 'angle' degrees clockwise
					   (ccw if angle is negative) from current
                                           orientation (i.e. rotation is not w.r.t.
                                           a fixed reference direction).

 					   Note that rotation takes time!


	- Important constants

	  G_ACCEL = 8.87	- Gravitational acceleration on Venus
	  MT_ACCEL = 35.0	- Max acceleration provided by the main thruster
	  RT_ACCEL = 25.0	- Max acceleration provided by right thruster
	  LT_ACCEL = 25.0	- Max acceleration provided by left thruster
          MAX_ROT_RATE = .075    - Maximum rate of rotation (in radians) per unit time

	- Functions you need to analyze and possibly change

	  * The Lander_Control(); function, which determines where the lander should
	    go next and calls control functions
          * The Safety_Override(); function, which determines whether the lander is
            in danger of crashing, and calls control functions to prevent this.

	- You *can* add your own helper functions (e.g. write a robust thruster
	  handler, or your own robust sensor functions - of course, these must
	  use the noisy and possibly faulty ones!).

	- The rest is a black box... life sometimes is like that.

        - Program usage: The program is designed to simulate different failure
                         scenarios. Mode '1' allows for failures in the
                         controls. Mode '2' allows for failures of both
                         controls and sensors. There is also a 'custom' mode
                         that allows you to test your code against specific
                         component failures.

			 Initial lander position, orientation, and velocity are
                         randomized.

	  * The code I am providing will land the module assuming nothing goes wrong
          with the sensors and/or controls, both for the 'easy.ppm' and 'hard.ppm'
          maps.

	  * Failure modes: 0 - Nothing ever fails, life is simple
			   1 - Controls can fail, sensors are always reliable
			   2 - Both controls and sensors can fail (and do!)
			   3 - Selectable failure mode, remaining arguments determine
                               failing component(s):
                               1 - Main thruster
                               2 - Left Thruster
                               3 - Right Thruster
                               4 - Horizontal velocity sensor
                               5 - Vertical velocity sensor
                               6 - Horizontal position sensor
                               7 - Vertical position sensor
                               8 - Angle sensor
                               9 - Sonar

        e.g.

             Lander_Control easy.ppm 3 1 5 8

             Launches the program on the 'easy.ppm' map, and disables the main thruster,
             vertical velocity sensor, and angle sensor.

		* Note - while running. Pressing 'q' on the keyboard terminates the
			program.

        * Be sure to complete the attached REPORT.TXT and submit the report as well as
          your code by email. Subject should be 'C85 Safe Landings, name_of_your_team'

	Have fun! try not to crash too many landers, they are expensive!

  	Credits: Lander image and rocky texture provided by NASA
		 Per Parker spent some time making sure you will have fun! thanks Per!
*/

/*
  Standard C libraries
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "Lander_Control.h"

//Keep track of the power output of each thruster
double m_pwr;
double l_pwr;
double r_pwr;
int VX_OK = 1;
int VY_OK = 1;
int PX_OK = 1;
int PY_OK = 1;
int S_OK = 1;
double sp = 0.01;
double sonar[36];

void Lander_Control(void)
{
 /*
   This is the main control function for the lander. It attempts
   to bring the ship to the location of the landing platform
   keeping landing parameters within the acceptable limits.

   How it works:

   - First, if the lander is rotated away from zero-degree angle,
     rotate lander back onto zero degrees.
   - Determine the horizontal distance between the lander and
     the platform, fire horizontal thrusters appropriately
     to change the horizontal velocity so as to decrease this
     distance
   - Determine the vertical distance to landing platform, and
     allow the lander to descend while keeping the vertical
     speed within acceptable bounds. Make sure that the lander
     will not hit the ground before it is over the platform!

   As noted above, this function assumes everything is working
   fine.
*/

/*************************************************
 TO DO: Modify this function so that the ship safely
        reaches the platform even if components and
        sensors fail!

        Note that sensors are noisy, even when
        working properly.

        Finally, YOU SHOULD provide your own
        functions to provide sensor readings,
        these functions should work even when the
        sensors are faulty.

        For example: Write a function Velocity_X_robust()
        which returns the module's horizontal velocity.
        It should determine whether the velocity
        sensor readings are accurate, and if not,
        use some alternate method to determine the
        horizontal velocity of the lander.

        NOTE: Your robust sensor functions can only
        use the available sensor functions and control
        functions!
	DO NOT WRITE SENSOR FUNCTIONS THAT DIRECTLY
        ACCESS THE SIMULATION STATE. That's cheating,
        I'll give you zero.
**************************************************/

 double VXlim;
 double VYlim;

 // Set velocity limits depending on distance to platform.
 // If the module is far from the platform allow it to
 // move faster, decrease speed limits as the module
 // approaches landing. You may need to be more conservative
 // with velocity limits when things fail.
 if (fabs(Position_X_robust()-PLAT_X)>200) VXlim=25;
 else if (fabs(Position_X_robust()-PLAT_X)>100) VXlim=15;
 else VXlim=5;

 if (PLAT_Y-Position_Y_robust()>200) VYlim=-20;
 else if (PLAT_Y-Position_Y_robust()>100) VYlim=-10;  // These are negative because they
 else VYlim=-4;				       // limit descent velocity

 // Ensure we will be OVER the platform when we land
 if (fabs(PLAT_X-Position_X_robust())/fabs(Velocity_X_robust())>1.25*fabs(PLAT_Y-Position_Y_robust())/fabs(Velocity_Y_robust())) VYlim=0;

 if (MT_OK == 0) {
 	if (RT_OK == 0) {
 		Land_LT_Only(VXlim, VYlim);
 	} else {
    Left_Thruster(0.0);
 		Land_RT_Only(VXlim, VYlim);
 	}

 } else {
 	if (LT_OK == 1 && RT_OK == 1) {
 		Land_MT_LT_RT(VXlim, VYlim);
 	} else {
 		Land_MT_Only(VXlim, VYlim);
 	}
 }

 // IMPORTANT NOTE: The code below assumes all components working
 // properly. IT MAY OR MAY NOT BE USEFUL TO YOU when components
 // fail. More likely, you will need a set of case-based code
 // chunks, each of which works under particular failure conditions.

 // Check for rotation away from zero degrees - Rotate first,
 // use thrusters only when not rotating to avoid adding
 // velocity components along the rotation directions
 // Note that only the latest Rotate() command has any
 // effect, i.e. the rotation angle does not accumulate
 // for successive calls.


}

void Safety_Override(void)
{
 /*
   This function is intended to keep the lander from
   crashing. It checks the sonar distance array,
   if the distance to nearby solid surfaces and
   uses thrusters to maintain a safe distance from
   the ground unless the ground happens to be the
   landing platform.

   Additionally, it enforces a maximum speed limit
   which when breached triggers an emergency brake
   operation.
 */

/**************************************************
 TO DO: Modify this function so that it can do its
        work even if components or sensors
        fail
**************************************************/

/**************************************************
  How this works:
  Check the sonar readings, for each sonar
  reading that is below a minimum safety threshold
  AND in the general direction of motion AND
  not corresponding to the landing platform,
  carry out speed corrections using the thrusters
**************************************************/

 double DistLimit;
 double Vmag;
 double dmin;

 // Establish distance threshold based on lander
 // speed (we need more time to rectify direction
 // at high speed)
 Vmag=Velocity_X_robust()*Velocity_X_robust();
 Vmag+=Velocity_Y_robust()*Velocity_Y_robust();

 DistLimit=fmax(75,Vmag);

 // If we're close to the landing platform, disable
 // safety override (close to the landing platform
 // the Control_Policy() should be trusted to
 // safely land the craft)
 if (fabs(PLAT_X-Position_X_robust())<150&&fabs(PLAT_Y-Position_Y_robust())<150) return;

 // Determine the closest surfaces in the direction
 // of motion. This is done by checking the sonar
 // array in the quadrant corresponding to the
 // ship's motion direction to find the entry
 // with the smallest registered distance


 // Everything works, let can let Paco's code do all the work

 //Check if Main Thrusters is working
 if (MT_OK == 0) {
   if (LT_OK == 0) {
     if (RT_OK == 1) {
     	RT_Only(DistLimit, Vmag, dmin);
     }
   } else {
     if (RT_OK == 0) {
     	LT_Only(DistLimit, Vmag, dmin);
     } else {
     	LT_RT_Only(DistLimit, Vmag, dmin);
     }
   }
 } else {
   if (LT_OK == 0) {
    Right_Thruster(0.0);
    MT_Only(DistLimit, Vmag, dmin);
     

   } else {
     if (RT_OK == 0) {
       MT_LT_Only(DistLimit, Vmag, dmin);
     } else {
       MT_LT_RT(DistLimit, Vmag, dmin);
      
     }
   }
 }

}


double convert_to_radians(double degree) {
	double radians = degree * (PI/180);
	return radians;
}

double vertical_range(double distance) {
	double vertical;
	if (distance != -1.0) {
	  vertical = distance * cos(convert_to_radians(Angle_Robust()));
	} else {
	  vertical = -1.0;
	}
	return vertical;

}

double horizontal_range(double distance) {
	double horizontal;
	if (distance != -1.0) {
		horizontal = distance * sin(convert_to_radians(Angle_Robust()));
	} else {
		horizontal = -1.0;
	}
	return horizontal;

}

void MT_LT_RT(double DistLimit, double Vmag, double dmin){
  // Horizontal direction.
  dmin=1000000;
  if (Velocity_X_robust()>0)
  {
   for (int i=5;i<14;i++)
    if (SONAR_DIST[i]>-1&&SONAR_DIST[i]<dmin) dmin=SONAR_DIST[i];
  }
  else
  {
   for (int i=22;i<32;i++)
    if (SONAR_DIST[i]>-1&&SONAR_DIST[i]<dmin) dmin=SONAR_DIST[i];
  }
  // Determine whether we're too close for comfort. There is a reason
  // to have this distance limit modulated by horizontal speed...
  // what is it?
  if (dmin<DistLimit*fmax(.25,fmin(fabs(Velocity_X_robust())/5.0,1)))
  { // Too close to a surface in the horizontal direction
   if (Angle_Robust()>1&&Angle_Robust()<359)
   {
    if (Angle_Robust()>=180) Rotate(360-Angle_Robust());
    else Rotate(-Angle_Robust());
    return;
   }

   if (Velocity_X_robust()>0){
    Right_Thruster(1.0);
    Left_Thruster(0.0);
   }
   else
   {
    Left_Thruster(1.0);
    Right_Thruster(0.0);
   }
  }

  // Vertical direction
  dmin=1000000;
  if (Velocity_Y_robust()>5)      // Mind this! there is a reason for it...
  {
   for (int i=0; i<5; i++)
    if (SONAR_DIST[i]>-1&&SONAR_DIST[i]<dmin) dmin=SONAR_DIST[i];
   for (int i=32; i<36; i++)
    if (SONAR_DIST[i]>-1&&SONAR_DIST[i]<dmin) dmin=SONAR_DIST[i];
  }
  else
  {
   for (int i=14; i<22; i++)
    if (SONAR_DIST[i]>-1&&SONAR_DIST[i]<dmin) dmin=SONAR_DIST[i];
  }
  if (dmin<DistLimit)   // Too close to a surface in the horizontal direction
  {
   if (Angle_Robust()>1||Angle_Robust()>359)
   {
    if (Angle_Robust()>=180) Rotate(360-Angle_Robust());
    else Rotate(-Angle());
    return;
   }
   if (Velocity_Y_robust()>2.0){
    Main_Thruster(0.0);
   }
   else
   {
    Main_Thruster(1.0);
   }
  }
}

void MT_LT_Only(double DistLimit, double Vmag, double dmin){
	l_pwr = PLAT_X - Position_X_robust();
	m_pwr = Position_X_robust() - PLAT_X;
	//Set power on the thrusters
	//Set power on the left thruster
	if(l_pwr < 0) l_pwr = 0;
	else if(Velocity_X_robust() > 0) l_pwr = 0;
	else if(Velocity_X_robust() < 0) l_pwr = (l_pwr) * Velocity_X_robust();
	if (l_pwr < 0) l_pwr = -l_pwr;
	if (l_pwr > 1) l_pwr = 1;

	//Set power on the main thruster
	if(m_pwr < 0) m_pwr = 0;
  	else if(Velocity_X_robust() < 0) m_pwr = 0;
  	else if(Velocity_X_robust() > 0) m_pwr = (m_pwr) * Velocity_X_robust();
  	if (m_pwr < 0) m_pwr = 0;
  	m_pwr = 2 * m_pwr;
  	if (m_pwr > 1) m_pwr = 1;

  	if ((PLAT_X - Position_X_robust()) < -5) {
  		// Need to go to the right
    	double theta = 315;
    	if (Angle_Robust()<=180) Rotate(360 + theta-Angle_Robust());
    	else Rotate(theta -Angle_Robust());
    	Main_Thruster(m_pwr);
  		Left_Thruster(l_pwr);

  		if(Velocity_Y_robust() < -10.0) {
    		Main_Thruster(0.5);
    	}
  	} else if ((PLAT_X - Position_X_robust()) > 5) {
  		//Need to go right
  		if (Angle_Robust()>=180) Rotate(360-Angle_Robust());
    	else Rotate(-Angle_Robust());
  		Left_Thruster(l_pwr);

  		if (Velocity_Y_robust() < -10.0) {
  			Main_Thruster(0.5);
  		}
  	} else {
  		MT_Only(DistLimit, Vmag, dmin);
  		Left_Thruster(0.0);

  	}

  	if (Velocity_Y_robust() > 2.0) {
  		Main_Thruster(0.0);
  		if (Angle_Robust() != 0) Left_Thruster(0.0);
  	} 

}


void MT_RT_Only(double DistLimit, double Vmag, double dmin){
  m_pwr = PLAT_X - Position_X_robust();
  r_pwr = Position_X_robust() - PLAT_X;
  if(r_pwr < 0) r_pwr = 0;
  else if (Velocity_X_robust() < 0) r_pwr = 0;
  else if (Velocity_X_robust() > 0) r_pwr = (r_pwr) * Velocity_X_robust(); 
  if (r_pwr < 0) r_pwr = -r_pwr;
  if (r_pwr > 1) r_pwr = 1;

  if(m_pwr < 0) m_pwr = 0;
  else if(Velocity_X_robust() > 0) m_pwr = 0;
  else if(Velocity_X_robust() < 0) m_pwr = (m_pwr) * Velocity_X_robust();
  if (m_pwr < 0) m_pwr = -m_pwr;
  m_pwr = 2 * m_pwr;
  if (m_pwr > 1) m_pwr = 1;
  if ((PLAT_X - Position_X_robust()) > 0){
    // Need to go to the right
    if (Angle() <= 90){
      Rotate(-Angle_Robust() + 90);
    }
    else if (Angle_Robust() >= 270){

      Rotate((360 -Angle_Robust()) + 90);
      
    }
    else Rotate(-Angle_Robust() + 90);
    Main_Thruster(m_pwr);

    if(Velocity_Y_robust() > 2.0) Right_Thruster(0.0);
    else if(Velocity_Y_robust() < -10.0) Right_Thruster(1.0);
    else Right_Thruster(r_pwr);
 }
  else if ((PLAT_X - Position_X_robust()) < 0){
    // Need to go to the left
  
    if (Angle_Robust()>=180) Rotate(360-Angle_Robust());
    else Rotate(-Angle_Robust());
    Right_Thruster(r_pwr);

    if(Velocity_Y_robust() < -10.0) {
    	Main_Thruster(0.5);
    }
  }
  else{
    if (Angle_Robust()>=180) Rotate(360-Angle_Robust());
    else Rotate(-Angle_Robust());
    Right_Thruster(0.0);
    Main_Thruster(m_pwr);
  }
 
}
void LT_RT_Only(double DistLimit, double Vmag, double dmin){
	double dist = PLAT_X - Position_X_robust();
	if (dist > 0) {
		//Need to go left
    Left_Thruster(0.0);
		RT_Only(DistLimit, Vmag, dmin);
	} else {
		//Need to go right
    Right_Thruster(0.0);
		LT_Only(DistLimit, Vmag, dmin);
	}
}
void MT_Only(double DistLimit, double Vmag, double dmin){
  double dist = PLAT_X - Position_X_robust();
  m_pwr = Velocity_X_robust() * (Position_X_robust() - PLAT_X) / 20;
  double theta;
  if (m_pwr < 0) m_pwr = -m_pwr;
  if (m_pwr > 1.0) m_pwr = 1.0;

  if(dist > 0){
    //Need to go right
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta > 90.0) theta = 90.0;
    else if (Velocity_X_robust() < 0) theta = 90.0;
    else if (theta < 10) theta = 10;

    if (Angle_Robust()>=180) Rotate(360 + theta -Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Main_Thruster(m_pwr * 0.5);
  }
  else if (dist < 0){
    //Need to go left
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = -90.0;
    else if (Velocity_X_robust() > 0) theta = -90.0;
    else if (theta > -10) theta = -10;
    if (Angle_Robust()>=180) Rotate(360 + theta -Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Main_Thruster(m_pwr * 0.5);
  }
  else{
    if (Angle_Robust()>=180) Rotate(360 -Angle_Robust());
    else Rotate(360 - Angle_Robust());
    if(Velocity_Y_robust() < -2.0) Main_Thruster(1.0);
    else Main_Thruster(m_pwr * 0.5);
  }

  if (Velocity_Y_robust() > 2.0) Main_Thruster(0.0);
  else if(Velocity_Y_robust() < -4.0) Main_Thruster(1.0);

}
void LT_Only(double DistLimit, double Vmag, double dmin){
  double dist = PLAT_X - Position_X_robust();
  l_pwr = (Position_X_robust() - PLAT_X) / 20;
  double theta;
  //Rotate(-90.0);
  if (l_pwr < 0) l_pwr = -l_pwr;
  if (l_pwr > 1.0) l_pwr = 1.0;


  if (Velocity_Y_robust()>2.0){
    Left_Thruster(0.0);
    return;
  }
  else if(Velocity_Y_robust() < -4.0){
    if (Angle_Robust() >= 270){

      Left_Thruster(1.0);
      Rotate(-Angle_Robust() + 270);
      return;
    }
    else if (Angle() <= 90){

      Rotate(-(360 + Angle_Robust()) + 270);
      Left_Thruster(1.0);
      return;
    }
    else Rotate(-Angle_Robust() + 270);
    Left_Thruster(1.0);

    return;
  }

  if(dist > 0){
    //Need to go right
    if(Velocity_X_robust() >= 10.0) l_pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = -90;
    else if (Velocity_X_robust() > 0) theta = 0;
    else if (theta > 0) theta = 0;

    if (Angle_Robust()>=180) Rotate(360 + theta - Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Left_Thruster(l_pwr);
  }
  else if (dist < 0){
    //Need to go left
    if(Velocity_X_robust() < -10.0) l_pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = 90;
    else if (Velocity_X_robust() > 0) theta = 0.0;
    else if (theta > 0) theta = 0;
    if (Angle_Robust()>=180) Rotate(theta - Angle_Robust() +180);
    else Rotate(-1 * (theta + Angle_Robust() -180));

    Left_Thruster(l_pwr);
  }
  else{
    if (Angle_Robust()>=270 || Angle_Robust()<=90) Rotate(Angle_Robust() - 270);
    else Rotate(270 - Angle());
    if(Velocity_Y_robust() < -2.0) Left_Thruster(1.0);
    else Left_Thruster(l_pwr);
  }
}
void RT_Only(double DistLimit, double Vmag, double dmin){
  double dist = PLAT_X - Position_X_robust();
  double r_pwr = Velocity_X_robust() * (Position_X_robust() - PLAT_X) / 20;
  double theta;

  if (r_pwr < 0) r_pwr = -r_pwr;
  if (r_pwr > 1.0) r_pwr = 1.0;

  if(Velocity_Y_robust() < -4.0){
    if (Angle_Robust() <= 90){
      Right_Thruster(1.0);
      Rotate(-Angle_Robust() + 90);
      return;
    }
    else if (Angle_Robust() >= 270){

      Rotate((360 -Angle_Robust()) + 90);
      Right_Thruster(1.0);
      return;
    }
    else Rotate(-Angle_Robust() + 90);
    Right_Thruster(1.0);
    return;
  }

  if(dist > 0){
    //Need to go right
    if(Velocity_X_robust() >= 10.0) r_pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta > 90.0) theta = 0.0;
    if (Velocity_X_robust() < 0) theta = 90;
    else if (theta < 0) theta = 90;
    if (Angle_Robust()>=180) Rotate(-Angle_Robust() + 180);
    else Rotate(180 - Angle_Robust());

    Right_Thruster(r_pwr);
  }
  else if (dist < 0){
    //Need to go left
    if(Velocity_X_robust() <= -10.0) r_pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = -90.0;
    else if (Velocity_X_robust() > 0) theta = 0.0;
    else if (theta > 0) theta = 0;
    if (Angle_Robust()>=270) Rotate(270 + theta - Angle_Robust());
    if (Angle_Robust()>=180) Rotate(360 + theta - Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Right_Thruster(r_pwr);
  }
  else{
    if (Angle_Robust()>=90 || Angle_Robust()<=270) Rotate(Angle_Robust() - 90);
    else Rotate(90 - Angle_Robust());
    if(Velocity_Y_robust() < -2.0) Right_Thruster(1.0);
    else Right_Thruster(r_pwr);
  }
  if (Velocity_Y_robust()>2.0){
    Right_Thruster(0.0);

  }

}

void Land_MT_LT_RT(double VXlim, double VYlim) {
 if (Angle_Robust()>1&&Angle_Robust()<359)
 {
  if (Angle_Robust()>=180) Rotate(360-Angle_Robust());
  else Rotate(-Angle_Robust());
  return;
 }

 // Module is oriented properly, check for horizontal position
 // and set thrusters appropriately.
 if (Position_X_robust()>PLAT_X)
 {
  // Lander is to the LEFT of the landing platform, use Right thrusters to move
  // lander to the left.
  Left_Thruster(0);	// Make sure we're not fighting ourselves here!
  if (Velocity_X_robust()>(-VXlim)) Right_Thruster((VXlim+fmin(0,Velocity_X_robust()))/VXlim);
  else
  {
   // Exceeded velocity limit, brake
   Right_Thruster(0);
   Left_Thruster(fabs(VXlim-Velocity_X_robust()));
  }
 }
 else
 {
  // Lander is to the RIGHT of the landing platform, opposite from above
  Right_Thruster(0);
  if (Velocity_X_robust()<VXlim) Left_Thruster((VXlim-fmax(0,Velocity_X_robust()))/VXlim);
  else
  {
   Left_Thruster(0);
   Right_Thruster(fabs(VXlim-Velocity_X_robust()));
  }
 }

 // Vertical adjustments. Basically, keep the module below the limit for
 // vertical velocity and allow for continuous descent. We trust
 // Safety_Override() to save us from crashing with the ground.
 if (Velocity_Y_robust()<VYlim) Main_Thruster(1.0);
 else Main_Thruster(0);

}

void Land_MT_Only(double VXlim, double VYlim) {

  double dist = PLAT_X - Position_X_robust();
  double pwr = Velocity_X_robust() * (Position_X_robust() - PLAT_X) / 20;
  double theta;
  if (pwr < 0) pwr = -pwr;
  if (pwr > 1.0) pwr = 1.0;

  if(dist > 0){
    //Need to go right
    theta = Velocity_X_robust() * 15.0 / 50;
    if(theta > 15.0) theta = 15.0;
    else if (Velocity_X_robust() < 0) theta = 15.0;
    else if (theta < 10) theta = 10;

    if (Angle_Robust()>=180) Rotate(360 + theta -Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Main_Thruster(pwr);
  }
  else if (dist < 0){
    //Need to go left
    theta = Velocity_X_robust() * 15.0 / 50;
    if(theta < -15.0) theta = -15.0;
    else if (Velocity_X_robust() > 0) theta = -15.0;
    else if (theta > -10) theta = -10;
    if (Angle_Robust()>=180) Rotate(360 + theta -Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Main_Thruster(pwr);
  }
  else{
    if (Angle_Robust()>=180) Rotate(360 -Angle_Robust());
    else Rotate(360 - Angle_Robust());
    if(Velocity_Y_robust() < -2.0) Main_Thruster(1.0);
    else Main_Thruster(pwr);
  }

  if (Velocity_Y_robust()>2.0){
    Main_Thruster(0.0);
  }
  else if(Velocity_Y_robust() < VYlim) Main_Thruster(1.0);

}

void Land_LT_Only(double VXlim, double VYlim) {
  double dist = PLAT_X - Position_X_robust();
  double pwr = (Position_X_robust() - PLAT_X) / 20;
  double theta;
  double height = PLAT_Y -  Position_Y_robust();
  double safe_height = Velocity_Y_robust() * Angle_Robust() * PI / 4 * (MAX_ROT_RATE * 180.0);
  if(Position_Y_robust() + 30 >= PLAT_Y){
    Left_Thruster(0.0);
    if (Angle_Robust()>=180) Rotate(360 -Angle_Robust());
    else Rotate(-Angle_Robust());
    return;
  }

  if (pwr < 0) pwr = -pwr;
  if (pwr > 1.0) pwr = 1.0;

  if (Velocity_Y_robust() >2.0){
    Left_Thruster(0.0);
    return;
  }
  else if(Velocity_Y_robust() < -4.0){
    if (Angle_Robust() >= 270){
      Left_Thruster(1.0);
      Rotate(-Angle_Robust() + 270);
      return;
    }
    else if (Angle_Robust() <= 90){
      Rotate(-(360 + Angle_Robust()) + 270);
      Left_Thruster(1.0);
      return;
    }
    else Rotate(-Angle_Robust() + 270);
    Left_Thruster(1.0);
    return;
  }

  if(dist > 0){
    //Need to go right
    if(Velocity_X_robust() >= 3.0) pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = -90;
    else if (Velocity_X_robust() > 0) theta = 0;
    else if (theta > 0) theta = 0;

    if (Angle_Robust()>=180) Rotate(360 + theta - Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Left_Thruster(pwr);
  }
  else if (dist < 0){
    //Need to go left
    if(Velocity_X_robust() < -3.0) pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = 90;
    else if (Velocity_X_robust() > 0) theta = 0.0;
    else if (theta > 0) theta = 0;
    if (Angle_Robust()>=180) Rotate(theta - Angle_Robust() +180);
    else Rotate(-1 * (theta + Angle_Robust() -180));

    Left_Thruster(pwr);
  }
  else{
    if (Angle_Robust()>=270 || Angle_Robust()<=90) Rotate(Angle_Robust() - 270);
    else Rotate(270 - Angle_Robust());
    if(Velocity_Y_robust() < -10.0) Left_Thruster(1.0);
    else Left_Thruster(0.0);
  }
}

void Land_RT_Only(double VXlim, double VYlim) {
  double dist = PLAT_X - Position_X_robust();
  double pwr = (Position_X_robust() - PLAT_X) / 20;
  double theta;
  double height = PLAT_Y -  Position_Y_robust();
  double safe_height = Velocity_Y_robust() * Angle_Robust() * PI / 4 * (MAX_ROT_RATE * 180.0);
  if(Position_Y_robust() + 30 >= PLAT_Y){
    Right_Thruster(0.0);
    if (Angle_Robust()>=180) Rotate(360 -Angle_Robust());
    else Rotate(-Angle_Robust());
    return;
  }

  if (pwr < 0) pwr = -pwr;
  if (pwr > 1.0) pwr = 1.0;

  else if(Velocity_Y_robust() < -4.0){
    if (Angle_Robust()>=90 || Angle_Robust()<=270) Rotate(90 - Angle_Robust());
    else if(Angle_Robust() > 270) Rotate(360 - Angle_Robust() + 90);
    else Rotate(90 - Angle_Robust());
    Right_Thruster(1.0);
    return;
  }
  else if(Velocity_Y_robust() > 2.0){
    Right_Thruster(0.0);
    return;
  }
  if(dist > 0){
    //Need to go right
    if(Velocity_X_robust() >= 3.0) pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = -90;
    else if (Velocity_X_robust() > 0) theta = 0;
    else if (theta > 0) theta = 0;

    if (Angle_Robust()>=180) Rotate(-Angle_Robust() + 180);
    else Rotate(180 - Angle_Robust());
    Right_Thruster(pwr);
  }
  else if (dist < 0){
    //Need to go left
    if(Velocity_X_robust() < -3.0) pwr = 0;
    theta = Velocity_X_robust() * 45.0 / 50;
    if(theta < -90.0) theta = 90;
    else if (Velocity_X_robust() > 0) theta = 0.0;
    else if (theta > 0) theta = 0;
    if (Angle_Robust()>=270) Rotate(270 + theta - Angle_Robust());
    if (Angle_Robust()>=180) Rotate(360 + theta - Angle_Robust());
    else Rotate(theta - Angle_Robust());

    Right_Thruster(pwr);
  }
  else{
    if (Angle_Robust()>=270 || Angle_Robust()<=90) Rotate(90 - Angle_Robust());
    else Rotate(Angle_Robust() + 90);
    if(Velocity_Y_robust() < -10.0) Right_Thruster(1.0);
    else Right_Thruster(0.0);
  }
}

double Velocity_X_robust() {
  double const snapshot = 0.3;
  double static prev;
  /*
  double total = 0;
  double static vxBuffer[snapshot];
  if(!vxBuffer){
    for(int i=0; i < snapshot; i++){
      vxBuffer[i] = 0.0;
    }
  }
  int static c;
  if(!c) c = 0;
  vxBuffer[c] = Velocity_X();
  c = (c + 1) % snapshot;
  for(int j=0; j < snapshot; j++){
    total += vxBuffer[j];
  }
  return total / snapshot;
  */
  
  double static x_output;
  if(!prev) prev = Velocity_X();

  double o = snapshot * Velocity_X() + (1.0 - snapshot) * x_output;
  //if(abs(Velocity_X()) > abs(prev + Acceleration_X())) VX_OK = 0;
  if(VX_OK) prev = Velocity_X();
  else{
    
  }
  x_output = o;
  return o;
}

double Velocity_Y_robust() {
	double const snapshot = 0.3;
  double static prevP;
  double static prevV;
  double static y_output;
  if(!prevV) prevV = Velocity_Y();
  if(!prevP) prevP = Position_Y_robust();
  double o = snapshot * Velocity_Y() + (1.0 - snapshot) * y_output;
  if(abs(Velocity_X()) > abs(prevV + (Position_Y_robust() - prevP) / sp)) VY_OK = 0;
  if(VY_OK){ 
    prevV = Velocity_Y();
    y_output = o;
  }
  else{
    //Broken Sensor. The Horror
    y_output = prevV + (Position_Y_robust() - prevP) / sp;
    prevV = y_output;
  }
  prevP = Position_Y_robust();
  
  return y_output;
}

double Position_X_robust() {
  double const snapshot = 0.3;
  double static x_output;
  double static prevP;
  double static prevV;
  double vc = Velocity_X_robust();
  if(!x_output) x_output = Position_X();
  if(!prevP) prevP = Position_X();
  if(!prevV) prevV = Velocity_X();
  //Check if this is a bad sensor
  if(Position_X() < 0 || Position_X() > 1024) PX_OK = 0;
  //if(Position_X() > prevP + vc * sp / 2 + prevV * sp / 2) PX_OK = 0;
  //if(Position_X() < prevP - vc * sp / 2 + prevV * sp / 2) PX_OK = 0;
  x_output = snapshot * Position_X() + (1.0 - snapshot) * x_output;
  
	if(PX_OK){ 
    x_output = snapshot * Position_X() + (1.0 - snapshot) * x_output;
    prevP = x_output;
    prevV = Velocity_X_robust();
  }
  else{
    // Broken Sensor. The Horror
    
    
    x_output = prevP + vc * sp / 2 + prevV * sp / 2;
    prevP = x_output;
    prevV = Velocity_X_robust();
  }
  return x_output;
}

double Position_Y_robust() {
  double const snapshot = 0.3;
  double static threshold;
  double static prevP;
  double static prevV;
  double static y_output;
  if(!prevP) prevP = Position_Y();
  if(!prevV) prevV = Velocity_Y();
  double o = snapshot * Position_Y() + (1.0 - snapshot) * y_output;
  if(Position_Y() < 0 || Position_Y() > 1024) PY_OK = 0;
  if(Position_Y() < prevP + Velocity_Y() * sp - G_ACCEL * pow(sp, 2)) PY_OK = 0; 
  if(PY_OK){ 
    prevV = Velocity_Y();
    prevP = Position_Y(); 
    y_output = o;
  }
  else{
    //Borken Sensor. The Horror
    y_output = prevP + prevV * sp / 2 + Velocity_Y() * sp / 2;
    prevP = y_output;
    prevV = Velocity_Y();
  }
  
  return y_output;
}

double Angle_Robust() {
        // MAX_ROT_RATE
        double const w = 0.5;
        double static output;
        double a;
        if(Angle() < 0) a = 0;
        else if (Angle() > 360) a = 360.0;
        else a = Angle();
        if(!output) output = a;
        output = w * a + (1.0 - w) * output;
        return output;
}

double Acceleration_X(){
  //Find the horizontal accelaration 
  //Only requires Angle sensor to work properly!
  double m =  m_pwr * sin(Angle_Robust()) * MT_ACCEL;
  double r = r_pwr * cos(Angle_Robust()) * RT_ACCEL; 
  double l = l_pwr = -1 * l_pwr * cos(Angle_Robust()) * LT_ACCEL;
  return m + r + l;
}
double Acceleration_Y(){
  //Find the vertical accelaration
  //Only requires Angle sensor to work properly!
  double m =  m_pwr * cos(Angle_Robust()) * MT_ACCEL;
  double r = r_pwr * sin(Angle_Robust()) * RT_ACCEL; 
  double l = l_pwr = -1 * l_pwr * sin(Angle_Robust()) * LT_ACCEL;
  return m + r + l - G_ACCEL;
}

void Sonar_Robust(){
  double w = 0.8;
  for(int i=0; i < 36; i++){
     if(!sonar[i]) sonar[i] = SONAR_DIST[i];
     else{
       if(S_OK) sonar[i] = w * SONAR_DIST[i] + (1.0 - w) * sonar[i];
     }
  }
}